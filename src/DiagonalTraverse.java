import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DiagonalTraverse {

    /* o16

https://leetcode.com/discuss/interview-question/346342/Facebook-or-Onsite-or-Matrix-Antidiagonal-Traverse
 FB interview question

 idea: Simple: for each diagonal list, all elements have this: i+j remains same.

 we will have m+n lists in answer.

 dont try to traverse in the way answer is generated.

 At each index, we know WHICH list that index BELONGS to  -> number(i,j)  belongs to list(i+j)

if we keep adding top2bot and l2r (normal nested for loop), we will add them to their corresponding lists in correct order
 */
    public static List<List<Integer>> antiDiagonal(int[][] mat) {
        List<List<Integer>> ans = new ArrayList();
        if (mat.length == 0)
            return ans;
        int m = mat.length, n = mat[0].length;

        // first make all lists. we need m+n-1 lists
        for (int i=1;i<m+n;i++)
            ans.add(new ArrayList());

        for (int i=0;i<m;i++)
            for (int j=0;j<n;j++)
                ans.get(i+j).add(mat[i][j]);

        return ans;
    }





    // lc 498
    /*
    ASK which direction we START with: up & right.  or   down & left
    here is UP & RIGHT

    general idea: walk in dir -> if you hit a wall, flip dir

    while loop from 0,0 to m-1,n-1
        each time add dir to i & j
        check if hit the wall -> change dir and  move i & j
    */

    public static int[] findDiagonalOrder(int[][] mat){
        if (mat.length == 0)
            return new int[]{};
        int m = mat.length, n = mat[0].length;
        // directions. 0:up&right 1:down&left
        int[][] dirs = {{-1,1},{1,-1}};
        int[] ans = new int[m*n];
        // index in ans
        int ians = 0;
        // direction index. starting to go up
        int d = 0;

        // i,j in mat
        int i = 0, j = 0;
        while(ians < m*n){
            ans[ians++] = mat[i][j];
            // move i&j   -> first move, then make sure the move was ok!
            i += dirs[d][0];
            j += dirs[d][1];

            // crossed the bottom floor going down&left -> fix by i:back to floor j: 2 forward (1fix+1forward)  // IMPORTANT: this will handle the bottom-left corner case!!
            if (i == m){
                i = m-1; // back to floor
                j +=2;
                d = 1 - d;
            }
            // crossed the right wall going up&right -> fix by j:back to right wall i: 2 forwrd (1fix+1forward)               // IMPORTANT: this will handle the top-right corner case!!
            if (j == n){
                j = n-1;
                i +=2;
                d = 1 - d;
            }
            // this means we went up and right too much. we should have only gone right
            if (i < 0 ){
                i = 0;
                d = 1 - d;
            }
            // this means we went down and left too much. we should have only gone down
            if (j<0){
                j = 0;
                d = 1-d;
            }
        }

        return ans;
    }


    // not by me. using as reference
    public static List<List<Integer>> antiDiagonalRef(int[][] matrix) {
        List<List<Integer>> result = new ArrayList<>();
        int m = matrix.length;
        int n = matrix[0].length;
        for(int i=0;i<m;i++){
            for(int j=0;j<n;j++){
                int ind=i+j;
                if(result.size()<=ind)
                    result.add(new ArrayList());
                result.get(ind).add(matrix[i][j]);
            }
        }
        return result;
    }
    public static void main(String[] args) {
        int[][] matrix0 = {
                { 1, 2, 3 },
                { 4, 5, 6 },
                { 7, 8, 9 }};
        int[][] matrix1 = new int[][]{{12, 7, 21, 31, 11},
                {45, -2, 14, 27, 19},
                {-3, 15, 36, 71, 26},
                {4, -13, 55, 34, 15}};

        int[][] matrix2 = new int[][]{{1, 2, 3, 1, 2, 3},
                {4, 5, 6, 4, 5, 6},
                {7, 8, 9, 7, 8, 9}};

        int[][] matrix3 = new int[][]{{1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9},
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}};

        int[][] matrix4 = new int[][]{{}};
        int[][] matrix5 = new int[][]{
                {12, 7, 21, 31, 11, 1, 2, 3, 1, 2, 3,1, 2, 3, 1, 2, 3},
                {45, -2, 14, 27, 19, 1, 2, 3, 1, 2, 3,1, 2, 3, 1, 2, 3},
                {-3, 15, 36, 71, 26, 1, 2, 3, 1, 2, 3,1, 2, 3, 1, 2, 3},
                {4, -13, 55, 34, 15, 1, 2, 3, 1, 2, 3,1, 2, 3, 1, 2, 3}};
        int[][][] inputs = {matrix0,matrix1,matrix2,matrix3,matrix4,matrix5};

        for (int[][] mat : inputs) {
            System.out.println(Arrays.toString( findDiagonalOrder(mat)));
            System.out.println(antiDiagonal(mat));
            System.out.println(antiDiagonalRef(mat));

        }


    }
}
