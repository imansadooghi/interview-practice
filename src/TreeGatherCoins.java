/*
*   o15
*  https://leetcode.com/discuss/interview-question/559790/Facebook-Telephonic-Interview
*
*  n-ary tree. with nodes (branch or leaf) that might have coins. find the steps it takes to gather all the coins and come back to node2root
*  only walking to the ones with coins.
*  if no coins -> return -1
*

Define no coin coming back as -1

define steps from here to coins in my subtrees

for children of each Node: call f()
    if NOT returned -1 => add steps +2  for going rountrip to child

at end: if there were any coins within self or children -> return them, otherwise -1

* */
public class TreeGatherCoins {
    public int coinSteps(Tree root){
        if (root == null)
            return -1;
        int steps = 0;
        if (root.children != null) {
            for (Tree child : root.children) {
                int s = coinSteps(child);
                if (s > -1)
                    steps += s + 2; //2 more for getting to this children and back
            }
        }
        // no steps means no child had coins,  and  if root itself doesnt have coin either => -1
        return (steps == 0 && !root.hasCoin) ? -1 : steps;
    }



    public class Tree{
        boolean hasCoin;
        Tree[] children;

        public Tree(boolean hasCoin, Tree[] children) {
            this.hasCoin = hasCoin;
            this.children = children;
        }
    }



    public static void main(String[] args) {
        TreeGatherCoins gc = new TreeGatherCoins();

        Tree ch11 = gc.new Tree(false,null);
        Tree ch1 = gc.new Tree(false, new Tree[]{ch11});

        Tree ch21 = gc.new Tree(false,null);
        Tree ch22 = gc.new Tree(true,null);
        Tree ch2 = gc.new Tree(true, new Tree[]{ch21,ch22});

        Tree ch3 = gc.new Tree(true,null);

        Tree ch41 = gc.new Tree(true,null);
        Tree ch42 = gc.new Tree(false,null);
        Tree ch43 = gc.new Tree(true,null);
        Tree ch44 = gc.new Tree(true,null);
        Tree ch4 = gc.new Tree(false,new Tree[]{ch41,ch42,ch43,ch44});

        Tree root = gc.new Tree(true,new Tree[]{ch1,ch2,ch3,ch4});

        System.out.println(gc.coinSteps(root));

    }

}
