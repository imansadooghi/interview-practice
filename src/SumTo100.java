import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SumTo100 {
    /*
https://leetcode.com/discuss/interview-question/357345/Facebook-or-Phone-Screen-or-Sum-to-100

similar to Target Sum:
https://leetcode.com/problems/target-sum/
    *
Add the mathematical operators + or - before any of the digits in the decimal numeric string 123456789
such that the resulting mathematical expression adds up to 100. Return all possible solutions.

There are 12 solutions:

1+2+3-4+5+6+78+9
1+2+34-5+67-8+9
1+23-4+5+6+78-9
1+23-4+56+7+8+9
12+3+4+5-6-7+89
12+3-4+5+67+8+9
12-3-4+5-6+7+89
123+4-5+67-89
123+45-67+8-9
123-4-5-6-7+8-9
123-45-67+89
-1+2-3+4+5+6+78+9
    * */
    private static final String DIGITS = "123456789";

    // backtracking: go from first -> at each step do - or +
    public static List<String> sumToBT(int target){
        List<String> ans = new ArrayList<>();
        waysBT(0,target,"",ans);
        return ans;
    }

    public static void waysBT(int b, int tgt, String expression, List<String> ans){
        if (b == DIGITS.length()){
            if (tgt == 0) {
                // Needed if question wants to not to use + at beginning
                if (expression.charAt(0) == '+')
                    ans.add(expression.substring(1));
                else
                    ans.add(expression);
            }
            return;
        }

        for (int i = b; i < DIGITS.length(); i++) {
            String num = DIGITS.substring(b,i+1);
            waysBT(i+1,tgt - Integer.valueOf(num), expression + "+" + num,ans);
            waysBT(i+1,tgt + Integer.valueOf(num), expression + "-" + num,ans);
        }
    }

    public static void main(String[] args) {
        for(String s : sumToBT(9))
            System.out.println(s);
    }



    //    public static List<String> sumTo(int target) {
//        // key: "i:sum" value: [exp1,exp2,...]     e.g.  k:"2:6"  v:["1+2+3"]
//        Map<String,List<String>> ways = new HashMap<>();
//        return
//    }
//
//    public static List<String> ways(int b, int tgt, String expression, Map<String,List<String>> ways) {
//        if (b == -1){
//            if (tgt == 0)
//
//        }
//    }
}
