public class WoodCutting {
    /*
    https://leetcode.com/discuss/interview-question/354854/Facebook-or-Phone-Screen-or-Cut-Wood

    Given an int array wood representing the length of n pieces of wood and an int k.
    It is required to cut these pieces of wood such that more or equal to k pieces of the same length len are cut.
    What is the longest len you can get?

Example 1:

Input: wood = [5, 9, 7], k = 3
Output: 5
Explanation:
5 -> 5
9 -> 5 + 4
7 -> 5 + 2

Example 2:

Input: wood = [232, 124, 456], k = 7
Output: 114

----------
Solution:
    we want to be greedy. Best we can get is -> if shortest wood is the length of our k wood logs.
    so call a checkerFunction to check if there are k pieces with CutLen available.

    in a loop start from range l = 1  and r = shortestWood (that is biggest possible)
    and binarySearch until find a CutLen possible

TODO: Wrong: we can pick all the woods from one long wood! example: [5,7,120], k=3 => answer is 40. shortest wood here will not be used at all  
*/
    public static int cutWood(int[] n, int k){
        // rightBound cannot be the shortestWood. has to be longest.
        int shortestWood = n[0];
        int sum = 0; // sum only for checking if answer exists
        for (int w : n) {
            sum += w;
            shortestWood = Math.min(shortestWood, w);
        }

        // if total length of woods is less than l => we cant even get k 1 meter woods
        if (sum < k)
            return 0;

        int l=1, r=shortestWood, mid = 0;
        while (l<r){
            mid = (l+r)/2;
            if (isCuttable(n,mid,k)){
                l = mid+1;
            } else {
                r = mid;
            }
        }

        return mid;
    }

    // w/cutlen gives number of logs with length cutLen
    static boolean isCuttable(int[] n, int cutlen, int k){
        int woodCount = 0;
        for (int w : n)
            woodCount += w/cutlen;
        return (woodCount>=k);
    }

    public static void main(String[] args) {
        int[] n = {124,232,456};
        System.out.println(cutWood(n,7));
    }
}
