import java.util.*;

public class MergeSortedSubarrays {
    //https://leetcode.com/discuss/interview-question/602376/Facebook-or-Phone-or-Success
    // special case of merge k sorted arrays. but all in a single array

    /*
    *    idea:
    *           - add all end Indexes to a hashSet
    *           - add all beginning indexes to PQ   special comparator to compare vals, not indexes
    *
    *           if q.poll() is an endIndex -> dont add its next
    * */
    public static int[] merge(int[] n){
        Set<Integer> ends = new HashSet<>();
        if (n.length == 0)
            return n;

        PriorityQueue<Integer> pq = new PriorityQueue<Integer>((i,j) -> (n[i]-n[j]));
        // 0 index is  beginning of first subarray
        pq.add(0);

        for (int i = 1; i < n.length; i++) {
            if (n[i] < n[i-1]) {
                ends.add(i - 1);
                pq.add(i);
            }
        }
        // last index is end of last Subarray!  also it was not added!
        ends.add(n.length-1);

        int[] ans = new int[n.length];
        int i = 0;
        while(!pq.isEmpty()){
            int topInd = pq.poll();
            ans[i++] = n[topInd];
            if (!ends.contains(topInd))
                pq.add(topInd+1);
        }

        return ans;
    }

    public static void main(String[] args) {
        System.out.println(Arrays.toString(merge(new int[]{1,2,5,3,13,190,100,190,192,0,2,7,190})));
        System.out.println(Arrays.toString(merge(new int[]{1,2,4,5,3,8,1,6,11})));
        System.out.println(Arrays.toString(merge(new int[]{1,2,4,5,3,8,1,6,0,123,10000,1,2,3,4})));
        System.out.println(Arrays.toString(merge(new int[]{ 1,2,3, 7,11,121, 4, 5, 21})));
        System.out.println(Arrays.toString(merge(new int[]{ 1,1})));
        System.out.println(Arrays.toString(merge(new int[]{ 1,0,3,-2})));
        System.out.println(Arrays.toString(merge(new int[]{ 5})));
        System.out.println(Arrays.toString(merge(new int[]{ })));



    }

    /*
    *  first find begins and ends of each Sorted Subarray.
    *  for begins:  add the INDEX of begin to pq.
    *  for ends(TRICKY): add them to a hashSet.  they are there to indicate the subarray has ended.
    *
    *  the do a while pq
    *   each time add actual value of topIndex of pq.
    *   and  ADD it if it is not the END OF ITS SUBARRAY. How? check in the hashSet!
    *
    *   add
    * */
}
