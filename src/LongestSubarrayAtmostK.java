import java.util.ArrayList;
import java.util.List;
import static org.junit.Assert.*;
// https://www.geeksforgeeks.org/longest-subarray-sum-elements-atmost-k/

/*
*
Given an array of integers, find the length of LONGEST subarray having sum of its elements atmost ‘k’ where k>0.

Input : arr[] = {1, 2, 1, 0, 1, 1, 0}, k = 4
Output : 5
Explanation:
 {1, 2, 1} => sum = 4, length = 3
 {1, 2, 1, 0}, {2, 1, 0, 1} => sum = 4, length = 4
 {1, 0, 1, 1, 0} =>5 sum = 3, length = 5

* */
public class LongestSubarrayAtmostK {

    // moving window. each time add from right. if addition has made it bigger than k, keep shortening from left until len<k  or window empty.
    static int longestSubarray(int[] nums, int k) {
        int l=0;
        int maxlen = 0, sum=0;
        for (int i=0; i<nums.length;i++) {
            sum += nums[i];
            while (l<=i && sum > k){
                sum -= nums[l];
                l++;
            }
            maxlen = Math.max(maxlen, i-l+1);
        }
    return maxlen;
    }

    public static void main(String[] args) {
        assertEquals(5,longestSubarray(new int[]{1,2,3,0,0,0,0,1,2},2));
        assertEquals(1,longestSubarray(new int[]{0},0));
        assertEquals(0,longestSubarray(new int[]{1},0));
        assertEquals(5,longestSubarray(new int[]{1, 2, 1, 0, 1, 1, 0},4));
        System.out.println("all passed");

        //assertEquals(longestSubarray(new int[]{},), );
    }
}
