package examples;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.*;

public class FutureExample {
    //ExecutorService executor = Executors.newSingleThreadExecutor();
    ExecutorService executor2 = Executors.newFixedThreadPool(10);

    Future<Integer> square(Integer i){
        return executor2.submit(() -> {
            Thread.sleep(new Random().nextInt(10) * 100);
            return i*i;
        });
    }

    public Future<String> calculateAsync() throws ExecutionException, InterruptedException {
        CompletableFuture<String> completableFuture
                = new CompletableFuture<>();

        Executors.newCachedThreadPool().submit(() -> {
            Thread.sleep(500);
            completableFuture.complete("Hello!");
            return null;
        });

        return completableFuture;
    }

    void demoCompletableFuture() throws ExecutionException, InterruptedException {
        ExecutorService executor = Executors.newFixedThreadPool(10);

        CompletableFuture<String> completableFuture = CompletableFuture.supplyAsync(() -> {
            try {
                TimeUnit.SECONDS.sleep(1);
                System.out.println("in supplyAsync");
            } catch (InterruptedException e) {
                throw new IllegalStateException(e);
            }
            return "hi ";
        }, executor);


        completableFuture.thenApply((input) -> "this input was given" + input);

        completableFuture.thenRun(() -> System.out.println("completableFuture example"));

        completableFuture.complete("called complete");
       // completableFuture.get();
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        FutureExample example = new FutureExample();
        List<Future> futures = new ArrayList<>();

        for (int i = 0; i < 10; i++) {
            futures.add(example.square(12));
        }
        Thread.sleep( 500);
        for (int i = 0; i < 10; i++) {
            System.out.println(i+": "+futures.get(i).isDone());
        }
        example.executor2.shutdown();

        //example.demoCompletableFuture();

        Future<String> completableFuture = example.calculateAsync();
        System.out.println(completableFuture.get());
    }
}


