package examples;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;

public class ForkJoinExample {
    private Set<String> visited = ConcurrentHashMap.newKeySet();
    String host = "";

    class FJT extends ForkJoinTask<String> {
        String url = "";

        public FJT(String url) {
            this.url = url;
        }

        @Override
        public String getRawResult() {
            return null;
        }

        @Override
        protected void setRawResult(String value) {

        }

        @Override
        protected boolean exec() {
            List<FJT> nextRound = new ArrayList<>();

            if (!visited.contains(url)){
                List<String> tobeCrawled = null;
                if (tobeCrawled != null){
                    for (String url : tobeCrawled){
                        if (url.startsWith(host) && !visited.contains(url))
                            nextRound.add(new FJT(url));
                    }
                }
            }

            ForkJoinTask.invokeAll(nextRound);

            return true;
        }
    }

    public List<String> f(){
        ForkJoinPool.commonPool().invoke(new FJT("x"));
        return new ArrayList<>(visited);
    }
}