package examples;

import javafx.collections.transformation.SortedList;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.*;

public class PlayGround {

    public static List<Integer> findAnagrams(String s, String p) {
        int[] pc = new int[26];
        for (int i=0;i<p.length();i++)
            pc[p.charAt(i) - 'a']++;

        List<Integer> ans = new ArrayList();
        int l=0, m= 0;
        for (int i=0;i<s.length();i++){
            pc[s.charAt(i)-'a']--;

            if (pc[s.charAt(i)-'a'] >= 0 ) // if came from positive to 0
                m++;

            if (m == p.length())
                ans.add(l);

            if (i-l+1 == p.length()){ // shorten the window
                pc[s.charAt(l++)-'a']++;
                if (pc[s.charAt(l)-'a'] > 0)
                    m--;
            }
        }

        return ans;
    }

    static void merge(int[] A, int[]B) {
        int n = A.length - 1;
        int m = B.length - 1 - A.length;
        int end = B.length - 1;

        while (end >= 0) {
            if (n < 0)
                B[end--] = B[m--];
            else if (m < 0)
                B[end--] = A[n--];
            else if (A[n] > B[m])
                B[end--] = A[n--];
            else
                B[end--] = B[m--];
        }
    }
    public static void main(String[] args) {

//        System.out.println(findAnagrams("cbaebabacd","abc"));
//        TreeSet<Integer> set = new TreeSet<>();
//        for (int i=0;i<10;i++)
//            set.add(i);
//
//        //set.iterator().next()
//        for (int x : set)
//            System.out.println(set.higher(x));
//
//        BigDecimal x = new BigDecimal(2);
//        x.divide(x);
//
//       // System.out.println(":"+String.join("", Collections.nCopies(5, " "))+":");
//        StringBuilder sb = new StringBuilder();
//        sb.append(123);
//        sb.insert(2,'d');
//        System.out.println(sb.toString());
//        "".compareTo("");
//        int[] a ={1,2,50,55,100,111,121};
//        int[] b = {3,6,9,55,121,4,2,1,1,1,1,5};
//        merge(a,b);
//        System.out.println(Arrays.toString(b));
    }



}




