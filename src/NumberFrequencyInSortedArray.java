public class NumberFrequencyInSortedArray {

    /*
    * https://leetcode.com/discuss/interview-question/124724/
    *
    *
Given a sorted array of n elements, possibly with duplicates, find the number of occurrences of the target element.

Example 1:

Input: arr = [4, 4, 8, 8, 8, 15, 16, 23, 23, 42], target = 8
Output: 3

O(logn) ans
    *
    * */
    public int count(int[] arr, int target) {
        int left = binarySearch(arr, target, true);
        if (left < 0) return 0;
        int right = binarySearch(arr, target, false);
        return right - left + 1;
    }

    private int binarySearch(int[] arr, int target, boolean leftmost) {
        int lo = 0;
        int hi = arr.length - 1;
        int idx = -1;
        while (lo <= hi) {
            int mid = (lo + hi) >>> 1; // avoid overflow. same as (lo + hi) / 2

            if (target > arr[mid]) {
                lo = mid + 1;
            } else if (target < arr[mid]) {
                hi = mid - 1;
            } else {
                idx = mid;
                if (leftmost) {
                    hi = mid - 1;
                } else {
                    lo = mid + 1;
                }
            }
        }
        return idx;
    }
}
