import static org.junit.Assert.assertEquals;

// https://www.geeksforgeeks.org/maximum-sum-subarray-sum-less-equal-given-sum/
public class MaxSumSubarrayLessThanK {

    // MOVING WINDOW
    // each time add from right.  if addition has made it bigger than k, keep shortening from left until len<k  or window empty.
    static int maxSumSubarray(int[] nums, int k) {
        int l=0;
        int maxSum = 0, sum=0;
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            while (l <= i && sum > k)
                sum -= nums[l++];
            maxSum = Math.max(maxSum, sum);
        }

        return maxSum;
    }

    public static void main(String[] args) {
        assertEquals(4, maxSumSubarray(new int[]{1,2,3,0,0,0,0,1,2},4));
        assertEquals(0, maxSumSubarray(new int[]{0},0));
        assertEquals(1, maxSumSubarray(new int[]{1},2));
        assertEquals(3, maxSumSubarray(new int[]{1, 6, 1, 0, 1, 1, 0},5));
        assertEquals(5, maxSumSubarray(new int[]{1,2,3,4,5},11));
        System.out.println("all passed");

        //assertEquals(maxSumSubarray(new int[]{},), );
    }
}
