import javafx.util.Pair;

import java.util.Stack;

public class CandyCrush1D {
    /* o19
https://leetcode.com/discuss/interview-question/380650/Bloomberg-or-Phone-Screen-or-Candy-Crush/342029

Given a string, reduce the string by removing 3 or more consecutive identical characters.
You should greedily remove characters from left to right.

Follow-up:
What if you need to find the shortest string after removal?

Example for follow up:

Input: "aaabbbacd"
Output: "cd"
Explanation:
1. Remove 3 'b': "aaabbbacd" => "aaaacd"
2. Remove 4 'a': "aaaacd" => "cd"

------------

solution:

use a stack for char and one for count of consecutive ones

go through chars,  update stack top  until you reach a different char -> pop or do nothing depending on the count

for loop:
    - if stack empty or char has changed  => PUSH current char with a count 1
    - Else     current matches with top
        - if :  top of stack count is k or more AND  either: next Char is diff than current, we are at end of chars  => POP it
        - else  means there are more of same as current char to come => only update the count of Stack top
    */
    public static String crushCandies(String s, int k) {

        Stack<Character> stc = new Stack();
        Stack<Integer> sti = new Stack();

        for (int i=0;i<s.length();i++){
            char c = s.charAt(i);
            if (stc.isEmpty() || stc.peek() != c){
                stc.push(c);
                sti.push(1);
            } else {
                // remove if: rootCount is 2 or more and either this is end of string or  next char is different
                if (sti.peek() >= k-1 && (i == s.length()-1 || s.charAt(i+1) != c)){
                    sti.pop();
                    stc.pop();
                } else  // else: not removable cuz: either is there is only 1 same char before, or there is more to come, so you have to wait.
                    sti.push(sti.pop()+1);
            }
        }

        StringBuilder sb = new StringBuilder();
        while (!stc.isEmpty()){
            char c = stc.pop();
            int count = sti.pop();
            for( int i=0;i<count;i++)
                sb.append(c);
        }

        return sb.reverse().toString();
    }

    static class Candy {
        char value;
        int consecutiveOccurences = 1;
        Candy(char c) {
            value = c;
        }
    }

    private static String crushRef(String string) {
        if (string.length() < 3) return string; // base case

        Stack<Candy> stack = new Stack<>();
        stack.push(new Candy(string.charAt(0))); // initialize by adding first character

        for(int i = 1; i < string.length(); i++) {
            char prevChar = stack.isEmpty() ? ' ' : stack.peek().value;
            char currentChar = string.charAt(i);
            char nextChar = i == string.length()-1 ? ' ' : string.charAt(i+1);

            Candy candy = new Candy(currentChar);
            candy.consecutiveOccurences = prevChar == currentChar ? stack.peek().consecutiveOccurences + 1 : candy.consecutiveOccurences;

            if (candy.consecutiveOccurences >= 3 && currentChar != nextChar) {
                while(!stack.isEmpty() && stack.peek().value == currentChar) {
                    stack.pop();
                }
            } else {
                stack.push(candy);
            }
        }

        StringBuilder sb = new StringBuilder(stack.size());
        while(!stack.isEmpty()) sb.append(stack.pop().value);

        return sb.reverse().toString();
    }

    public static void main(String[] args) {
        String[] strings = { "daaabaaabbaad","baaabbbabbccccd",
                "bbbbbbbccdddc","aaabbbacd","ccddccdcaacabbbaaccaccddcdcddd","baaabbbabbccccd","aaabbbacd"};
        for (String s:strings) {
            System.out.println(crushCandies(s, 3));
            System.out.println(crushRef(s));
        }
    }
}
