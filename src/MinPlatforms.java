import java.util.Arrays;
import java.util.PriorityQueue;

public class MinPlatforms {

    // Function to find minimum number of platforms needed in the
    // station so to avoid any delay in arrival of any train.
    // similar to LC 253 Meeting Rooms II
    public static int minPlatforms(double[] ar, double[] de) {
        if (de.length == 0)
            return 0;
        Arrays.sort(ar);
        Arrays.sort(de);

        PriorityQueue<Double> pq = new PriorityQueue<>();
        pq.add(de[0]);

        for (int i = 1; i < ar.length; i++) {
            double lastDep = pq.peek();
            if (lastDep <= ar[i])
                pq.poll();
            pq.add(de[i]);
        }

        return pq.size();
    }

    public static void main(String[] args) {

        double[] arrival = { 2.00, 2.10, 3.00, 3.20, 3.50, 5.00 };
        double[] departure = { 2.30, 3.40, 3.20, 4.30, 4.00, 5.20 };
        System.out.println(minPlatforms(arrival,departure));


        double arr[] = {900, 940, 950, 1100, 1500, 1800};
        double dep[] = {910, 1200, 1120, 1130, 1900, 2000};
        System.out.println(minPlatforms(arr,dep));

        System.out.println(minPlatforms(new double[]{5,10,0,10,10,10},new double[]{10,9,20,15,12,17}));

    }

}