import java.util.ArrayList;
import java.util.List;

/*
* Count the unique elements in an Sorted Array.

Example:

Input: [1, 1, 1, 1, 2, 2, 2, 2, 5, 5, 5, 7, 7, 8, 8, 10]
Output = 6

need to find O(logn) answer!
* */
public class UniqueNumsInSortedArray {

    // idea: first check one ahead. then 2 then 4 then 8 ...
    public static int countUnique(int[] nums) {
        int count = 0;
        int idx = 0;
        while (idx < nums.length) {
            ++count;

            // exponential search to find the next elem
            int bound = 1;
            while (idx + bound < nums.length && nums[idx + bound] == nums[idx]) {
                bound *= 2;
            }

            idx = upper_bound(nums,
                    idx + bound / 2,
                    Math.min(idx + bound, nums.length),
                    nums[idx]
            );
        }

        return count;
    }

    private static int upper_bound(int[] nums, int lo, int hi, int val) {
        int count = hi - lo;
        while (count > 0) {
            int step = count / 2;
            int cur = lo + step;

            if (val >= nums[cur]) {
                lo = cur + 1;
                count -= step + 1;
            } else {
                count = step;
            }
        }
        return lo;
    }

    public static void main(String[] args) {
        System.out.println(countUnique(new int[]{1,1,1,1,1,1,2,2,2,2,2,2,2,2,3,3,3,3,3,4,4,4,5,6,7,8,9}));
    }

    // similar question: lc 228

    // o(logn) in good case.
    public List<String> summaryRanges(int[] nums) {
        List<String> list = new ArrayList<>();
        for (int i = 0, end; i < nums.length; i = end + 1) {
            end = help(nums, i, nums.length);
            if (i != end)
                list.add(nums[i] + "->" + nums[end]);
            else
                list.add(String.valueOf(nums[i]));
        }
        return list;
    }

    private int help(int[] nums, int l, int r) {
        while (l + 1 < r) {
            int m = (l + r) / 2;
            if (nums[m] - nums[l] == m - l)
                l = m;
            else
                r = m;
        }
        return l;
    }
}
