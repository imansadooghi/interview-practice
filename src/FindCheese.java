import java.util.HashSet;
import java.util.Set;

/*
*  https://leetcode.com/discuss/interview-question/633689/Facebook-or-PhoneVideo-or-Find-Cheese
*
*  Given a mouse with 2 APIs in a maze.
*  Design an algorithm to find a cheese in the maze using only the 2 given APIs shown below.
 *
 * 	* Finder method:
 * 	    IF exists and Exists Should return true and STAY AT that location
 * 	    OR return false if we can't find cheese and GO BACK the mouse back to where it started.
 *

WE NEED TO Implement getCheese()
-------------------------------

Assume Direction is ENUM.

recursively start from 0,0.
in recursion f():
    - check if cheese is Here -> return TRUE
    - for all 4 directions:
        - if havent moved and is moveable -> move
            - if result of that move was success -> DONE
            - else: MOVE BACK TO HERE. (next iteration of forloop  will try other directions)

 * */
public class FindCheese {
    // 0: wall  1: path  9: cheese
    int[][] room;

    public static int i, j;
    private final int[][] dirs = {{1,0},{-1,0},{0,1},{0,-1}};
    int currentDir;


    /**  Similar to RobotCleaner.
     *   easier cuz Direction comes with move.
     *
     *   Steps: CheckCheese & MarkVisited  then Move(in4dirs) & recurseF()  &  Move Back
     */

    public boolean getCheese() {
        return f(new HashSet<>(),0,0);
    }

    public boolean f(Set<String> visited, int x, int y){
        if (hasCheese())
            return true;

        visited.add(x+":"+y);

        for(Direction dir : Direction.values()){
            int nextX = x, nextY = y;
            Direction opposite = Direction.Up; // just to initialize

            switch (dir){
                case Down:
                    nextX += 1;
                    opposite = Direction.Up;
                    break;
                case Up:
                    nextX -= 1;
                    opposite = Direction.Down;
                    break;
                case Right:
                    nextY += 1;
                    opposite = Direction.Left;
                    break;
                case Left:
                    nextY -= 1;
                    opposite = Direction.Right;
                    break;
            }
            if (!visited.contains(nextX+":"+nextY) && move(dir)){
                // if found, no need to go back
                if (f(visited,nextX,nextY))
                    return true;
                //failed: move back. always true cuz we came from there
                move(opposite);
            }
        }

        return false;
    }








    public FindCheese(int[][] room, int i, int j) throws Exception {
            this.room = room;
            this.i = i;
            this.j = j;
            currentDir = 0;

            if (i < 0 || i >= room.length || j <0 || j >= room[0].length
                    || room[i][j] == 0)
                throw new Exception("invalid initial location i&j");
        }

    /**
     * Moves to one of the directions (left, right, up, down) and returns false if you can't move and true if you can.
     */
    public boolean move(Direction dir){
        int iNew = 0, jNew = 0;
        switch (dir){
            case Down:
                 iNew = i + dirs[0][0];
                 jNew = j + dirs[0][1];
                break;
            case Up:
                iNew = i + dirs[1][0];
                jNew = j + dirs[1][1];
                break;
            case Right:
                iNew = i + dirs[2][0];
                jNew = j + dirs[2][1];
                break;
            case Left:
                iNew = i + dirs[3][0];
                jNew = j + dirs[3][1];
                break;
        }


        if (iNew < 0 || iNew >= room.length || jNew <0 || jNew >= room[0].length
                || room[iNew][jNew] == 0)
            return false;
        else{
            i = iNew;
            j = jNew;
            return true;
        }
    }

    /**
     * Returns true if there is a cheese in the current cell.
     */
    public boolean hasCheese(){
        return room[i][j] == 9;
    };

    public static void main(String[] args) throws Exception {
        int[][] room = {
                {1,1,0,0,1,0,1,1},
                {1,0,0,0,1,0,0,9},
                {1,1,1,1,1,0,1,1},
                {0,0,1,0,1,1,0,1},
                {0,1,1,0,0,0,0,1},
                {0,1,0,0,0,1,1,1},
                {0,1,0,1,0,1,0,1},
                {0,1,1,1,1,1,0,1}};

        FindCheese mouse = new FindCheese(room,2,2);
        System.out.println("init loc:" + i+ ":"+j);
        System.out.println(mouse.getCheese());
        System.out.println("end loc:" + i+ ":"+j);

    }

    enum Direction{
        Up,
        Down,
        Left,
        Right
    }
}
