public class Buffer {
    /**
     * o22
     *  https://leetcode.com/discuss/interview-question/354889/Facebook-or-Onsite-or-Buffer
     *

Implement Buffer with the following api

     class Buffer {

     public Buffer(int capacity) {}

     * Transfers the content of the given source char array into this buffer.
     * Returns the the number of chars that were written into the buffer.

    public int write(char[] src) {}

    public char[] read(int n) {}
}
     Example:

        Buffer buf = new Buffer(5); // [. . . . .]
        buf.write([abc]); // => 3 [abc . .]
        buf.write([def]); // => 2 because the buffer is full, you can only write two chars [abcde]
        buf.read(3); // => [abc] [. . . de]
        buf.write([xyzabc]); // => 3 [xyzde]
        buf.read(8); // returns [dexyz] becuase 'de' was written first [. . . . .]


     *  related: lc 622  design Circular Queue
     -------

     solution:
     define start and end and size and cap

     */

    char[] buf;
    int st, end, cap, size;

    public Buffer(int capacity) {
        this.cap = capacity;
        st = 0;
        end = 0;
        size = 0;
        buf = new char[cap];
    }

    public int write(char[] src) {
        if (src.length == 0 || size == cap)
            return 0;

        int i = 0;
        for (; i < src.length; i++) {
            buf[end++] = src[i];
            size++;
            end = end % cap;
            if (size == cap)
                break;
        }

        return i;
    }

    public char[] read(int n) {
        n = (n>size) ? size : n;
        char[] res = new char[n];
        for (int i = 0; i < n; i++) {
            res[i] = buf[st++];
            size--;
            st = st % cap;
        }
        return res;
    }

    public static void main(String[] args) {
        Buffer t = new Buffer(5);
        t.write("abc".toCharArray());
        //System.out.println(t.read(1));
        t.write("def".toCharArray());
        System.out.println(t.read(3));
        t.write("xyzabc".toCharArray());
        System.out.println(t.read(8));
    }
}
