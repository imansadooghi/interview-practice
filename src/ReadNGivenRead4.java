import java.util.Arrays;

public class ReadNGivenRead4 {

    /*  O21

    given read4K implement readline()
    *
    *
     *Given a function read4k(char[] buf), returns a string which has <= 4096 characters

    If the string less than 4096 characters which means reached the end of file ”

    Use API read4096(), write a function  readline(char[] line)

    Requirement:

    #1 readline() returns when reading ‘\n’ or ”;

    #2 readline() may be called multiple times on a file, the return value  should be correct.

    #3 readline() may return char array longer than 4096 chars.

-------------------------
Solution:
difference between this and readII is:  bOffset is the beginning of newline, we start from it on each call.

    * */
    static int bOffset = 0, bufferSize = 500;
    static char[] buffer4k = new char[bufferSize];
    static int nChars = 0;

    public static int readLine(char[] buf) {
        int resInd = 0; // index on result buf
        int i = 0; // index on buffer4k  -> goes from bOffset to nchars
        for (; ; ) {

            if (bOffset < nChars) {
                for (i = bOffset; i < nChars; i++) {
                    if (buffer4k[i] == '\n' || buffer4k[i] == '\r') {
                        break;
                    }
                }

                int copySize = i - bOffset;
                System.arraycopy(buffer4k, bOffset, buf, resInd, copySize);

                resInd += copySize;
                bOffset = i + 1;
                // this means the reason the for boffset->nChars ended was finding an ENDLine char, not finishing the buffer
                if (i < nChars)
                    break;

            } else { // reload 4kbuffer
                nChars = read4k(buffer4k);
                bOffset = 0;
                if (nChars == 0)
                    break;
            }
        }
        return resInd;
    }

    // this one is returning a String of the line instead
    public static String readLine() {
        int i = 0; // index on buffer4k  -> goes from bOffset to nchars
        StringBuilder sb = new StringBuilder();
        for (; ; ) {

            if (bOffset < nChars) {
                for (i = bOffset; i < nChars; i++) {
                    if (buffer4k[i] == '\n' || buffer4k[i] == '\r') {
                        break;
                    }
                }

                int copySize = i - bOffset;
                sb.append(buffer4k, bOffset, copySize);

                bOffset = i + 1;
                if (i < nChars)
                    break;

            } else { // reload 4kbuffer
                nChars = read4k(buffer4k);
                bOffset = 0;
                if (nChars == 0)
                    break;
            }
        }
        return sb.toString();
    }


    /*
*
*  lc 157 Read N Characters Given Read4
The API: int read4(char *buf) reads 4 characters at a time from a file.

The return value is the actual number of characters read.
For example, it returns 3 if there is only 3 characters left in the file.
By using the read4 API, implement the function int read(char *buf, int n) that reads n characters from the file.
Note: The read function will only be called once for each test case.
* */
    public static int read(char[] buf, int n) {
        int i = 0;
        char[] r4buf = new char[4];
        while (true) {
            int rsize = read4(r4buf);
            for (int j = 0; j < rsize && i < n; j++) {
                buf[i++] = r4buf[j];
            }
            if (i == n || rsize < 4)
                break;
        }
        System.out.println(Arrays.toString(buf));

        return i;
    }



/* lc 158 Read N Characters Given Read4 II
* The read function may be called MULTIPLE times.
*
IDEA:   you need a object level objectBuffer. keep read4s in that. and copy from that to method objectBuffer.
        numChars -> number of valid chars in object objectBuffer.
        bufferIndex -> index of first unread char in object objectBuffer, ready to be served.

        each time, start copy from bufferIndex, to  numChars (that is number of valid chars in objectBuffer)   or  to N
         (Note that objectBuffer does not always have 4 valid chars. it has numChars valid chars.)

        if we are out of copy loop, it is either:
        - we consumed all chars in objectBuffer   -> so call read4 and load new ones
        - i == n and we are done

reference link:

https://github.com/acgtun/leetcode/blob/master/algorithms/java/Read%20N%20Characters%20Given%20Read4%20II%20-%20Call%20multiple%20times.java

*
 * */

    static char[] objectBuffer = new char[4];
    static int bi = 0; // index of objectBuffer from which there are still leftover chars
    //int nChars = 0; // size of last call's objectBuffer

    public static int readII(char[] buf, int n) {
        int i = 0;

        while (i < n) {
            // read what's available in the buffer from last read4
            while (bi < nChars && i < n)
                buf[i++] = objectBuffer[bi++];

            // out of loop cuz all chars were used
            if (bi == nChars) {
                nChars = read4(objectBuffer);
                bi = 0;
                // if nothing left in file to load, exit
                if (nChars == 0)
                    break;
            }
        }

        return i;
    }


    public static void main(String[] args) {
        System.out.println("file size:" + fileInput4k.length());

        // these 3 lines for readII
//        char[] buf = new char[100000];
//        System.out.println(readII(buf,51));
//        System.out.println(readII(buf,29));
        // thse for readline c++ style
//        char[] buf = new char[100000];
//        System.out.println(readLine(buf));
//        System.out.println(new String(buf));
//        buf = new char[100000];
//        System.out.println(readLine(buf));
//        System.out.println(new String(buf));
        while (true) {
            String s = readLine();
            if (s.length() == 0)
                break;
            System.out.println(s);
        }

    }

    static int r4indexHidden = 0;
    final static String fileInput =
            "0123456789" +
                    "0123456789" +
                    "0123456789" +
                    "0123456789" +
                    "0123456789" +
                    "0123456789" +
                    "0123456789" +
                    "0123456789" +
                    "0123456789";

    public static int read4(char[] buf) {
        int readsize = (fileInput.length() - r4indexHidden >= 4) ? 4 : fileInput.length() - r4indexHidden;
        for (int i = 0; i < readsize; i++) {
            buf[i] = fileInput.charAt(i + r4indexHidden);
        }
        r4indexHidden += readsize;

        return readsize;
    }

    static int r4kindexHidden = 0;
    final static String fileInput4k =
            "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "0123456789012345678901234567890123\n4567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "0123456789\n0123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901\n234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890\n123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "012345678901234567890123456789\n01234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "0123456789012345678\n9012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "0123456789012345678\n9012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "0123456789012345678901234567\n8901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345\n678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "012345678901234567890123\n45678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "01234567890123456789012345678901234567890123456750" +
                    "0123456789012345678901\n2345678901234567890123456";

    public static int read4k(char[] buf) {
        int readsize = (fileInput4k.length() - r4kindexHidden >= 500) ? 500 : fileInput4k.length() - r4kindexHidden;
        for (int i = 0; i < readsize; i++) {
            buf[i] = fileInput4k.charAt(i + r4kindexHidden);
        }
        r4kindexHidden += readsize;

        //System.out.println(Arrays.toString(buf));
        return readsize;
    }
}
