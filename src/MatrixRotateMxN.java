
public class MatrixRotateMxN {
    /*
    *   rotate Matrix MxN  not in place                                                      (last col)
    *                                                                                           x
    *    try an example first:    iNew,jNew goes to-> jNew, n-iNew      (0)  x,y,z   --->       y
    *                                                                                           z
    *
    *   MAIN DIFFERENCE is THIS IS M*N  not N*N
    * */
    public static int[][] rotate(int[][] mat){
        if (mat.length == 0)
            return mat;

        int cols = mat[0].length;
        int rows = mat.length;
        int [][] ans = new int[cols][rows];

        for (int i = 0; i < rows; i++) {
            for (int j = 0; j < cols; j++) {
                ans[j][rows-1 - i ] = mat[i][j];
            }
        }

        return ans;
    }
    public static void main(String[] args) {
        int[][] mat = {{1,2,3},{4,5,6},{1,2,3},{4,5,6},{1,2,3},{4,5,6},{1,2,3},{4,5,6},{1,2,3},{4,5,6}};
        for (int[] aMat : mat) {
            for (int j = 0; j < mat[0].length; j++) {
                System.out.print(aMat[j] + ", ");
            }
            System.out.println();
        }

        mat = rotate(mat);

        System.out.println("rotated:");

        for (int[] aMat : mat) {
            for (int j = 0; j < mat[0].length; j++) {
                System.out.print(aMat[j] + ", ");
            }
            System.out.println();
        }
    }
}

