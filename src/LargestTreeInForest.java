import java.util.*;

public class LargestTreeInForest {
    /*
    * Goldman interview. o20
 we have a forest, with child2parent map. return the largest tree. if equal, return the one with lower number
Map<Integer,Integer> map

2->5
1->2
3->4
4->6

return node2root of largest tree
    * */

    /*
    *  my solution: maybe it's union-find?
    *
    *  idea is: create a Map that maps each  Node to the root of its tree. !
    *          you can do dfs, until you get to the root. when found add to the  Node2ROot map.
    *           next time if a child of you looks for its root -> you can give it directly  => you only traverse to root once!
    *
    *           That node2Root map is actually our  Union-Find set here.  and root is representative of each tree.
    *
    *           depending on what the question asks, you can define another map with key as the Root and value being either: count of nodes, or actually all the nodes listed.
    *
    *
    * */
    static Map<Integer,Integer> node2root;
    static Map<Integer,Integer> rootCount;
    // this, if they asked for nodes of trees.
    static Map<Integer,List<Integer>> treesByRoot;
    static int largestTree(Map<Integer,Integer> node2parent) {

        node2root = new HashMap<>();
        rootCount = new HashMap<>();
        treesByRoot = new HashMap<>();

        for(int child : node2parent.keySet())
            getRoot(child,node2parent);

        int maxRoot = -1;
        for (int root : rootCount.keySet()){
            if (maxRoot == -1) {
                maxRoot = root;
                continue;
            }
            if (rootCount.get(root) > rootCount.get(maxRoot))
                maxRoot = root;
            else if (rootCount.get(root) == rootCount.get(maxRoot))
                maxRoot = Math.min(root,maxRoot);
        }

        System.out.println(rootCount.entrySet());
        System.out.println(treesByRoot.entrySet());

        return maxRoot;
    }

    static int getRoot(int node,Map<Integer,Integer> c2p){
        // if it is already detemined in a prev iteration
        if (node2root.containsKey(node))
            return node2root.get(node);
        // it is node2root of its tree
        if (!c2p.containsKey(node)) {
            node2root.put(node,node);
            rootCount.put(node,1);
            List<Integer> trees = new ArrayList<>();
            trees.add(node);
            treesByRoot.put(node,trees);
            return node;
        }

        int ancestor = getRoot(c2p.get(node),c2p);
        node2root.put(node,ancestor);
        rootCount.put(ancestor, rootCount.get(ancestor)+1);
        treesByRoot.get(ancestor).add(node);
        return ancestor;
    }

    public static void main(String[] args) {
        int[] ch = {2,1,3,5,4,0,6,7,11};
        int[] p =  {5,2,5,8,6,7,7,13,12};
        Map<Integer,Integer> c2p = new HashMap<>();
        for (int i = 0; i <ch.length ; i++)
            c2p.put(ch[i],p[i]);

        System.out.println(largestTree(c2p));
    }
}
