import java.util.*;

public class TwoSumBinaryTree {
/* O24
tags: twopair  pair
Given a binary tree and an integer K, return two nodes which are at different level and their sum is equal to T.

Constraints :

- Tree can have duplicate values.
- Incase more than one pair is available in the tree, then return any of the pair.

https://leetcode.com/discuss/interview-question/640705/Facebook-or-Phone-or-Two-sum-in-Binary-Tree

ANSWER:
    BFS Level-order -> check if map has t - node.val -> ans;   add node to map.   add children to Queue

    the ONLY trick: it asks for different levels =>  put all nodes of a level at once at end of level

* */
    public List<Node> twoSum(Node root, int t) {
        Map<Integer,Node> map = new HashMap<>();
        List<Node> ans = new ArrayList<>();

        Queue<Node> q = new LinkedList<>();
        q.add(root);

        while(!q.isEmpty()){
            int size = q.size();
            Map<Integer,Node> levelMap = new HashMap<>();
            for (int i = 0; i < size; i++) {
                Node n = q.poll();

                if (map.containsKey( t - n.val)){
                    ans.add(n);
                    ans.add(map.get(t - n.val));
                    return ans;
                }

                levelMap.put(n.val, n);

                if (n.left != null)
                    q.add(n.left);
                if (n.right != null)
                    q.add(n.right);
            }

            map.putAll(levelMap);
        }

        return ans;
    }

    class Node {
        Node left, right;
        int val;

        public Node(Node left, Node right, int val) {
            this.left = left;
            this.right = right;
            this.val = val;
        }
    }
}
