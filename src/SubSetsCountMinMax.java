import java.util.Arrays;

public class SubSetsCountMinMax {

    /* o23
    https://leetcode.com/discuss/interview-question/275785/facebook-phone-screen-count-subsets

Input:
Given an array A of
-positive
-sorted
-no duplicate
-integer

A positive integer k

Output:

Count of all such subsets of A,
Such that for any such subset S,
Min(S) + Max(S) = k
subset should contain atleast two elements
----------------------

Solutioon: if list is sorted ->
                going from left and right, -> we have min and max,
                so whenever we have n[l] + n[r] == k  => these two with/without nums in between them (these nums cant ruin min and max) can form all subsets
                r & l are mandatory -> in between is optional
         similar to twoSum using sort.



I sort just in case they say not sorted. there is no better way than sorting,
if you use hashmap, you wont know how many nums fall in between  min & max
    * */
    public static int countSubsets(int[] nums, int k){
        Arrays.sort(nums);

        int l = 0, r=nums.length-1;
        int subsets = 0;

        while(l<r){
            int sum = nums[l] + nums[r];
            if (sum == k) {
                //subsets += Math.pow(2, (r - l) - 1);
                subsets += 1 << ((r-l)-1);
                l++; r--;
            }else if( sum < k)
                l++;
            else
                r--;
        }
        return subsets;
    }

    /*
    https://leetcode.com/discuss/interview-question/268604/Google-interview-Number-of-subsets

    *  FOLLOW UP:                   Min(S) + Max(S) <= k
    *
    *  also, duplicates possible in nums
    * subsets of size 1 allowed
    *
    * not sure about why  2 to power of right-left:
    *   CUZ the right (upper-bound) is optional. but left MUST be included => anything from l+1 to r is optional => that is r-l
     *
    * */
    public static int countSubsetsII(int[] nums, int k){
        Arrays.sort(nums);

        int l = 0, r=nums.length-1;
        int subsets = 0;
        // important  cuz sub
        while(l<=r){
            int sum = nums[l] + nums[r];
            if (sum <= k) {
                //subsets += Math.pow(2, r - l );
                subsets += 1 << (r-l);

                l++;
            } else
                r--;
        }
        return subsets;
    }

    public static void main(String[] args) {
        System.out.println(countSubsets(new int[]{3,2,4,5,1},6));
        System.out.println(countSubsetsII(new int[]{2, 4, 5, 7},8));
    }
}
