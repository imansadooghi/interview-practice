package concept;

public class GarbageCollectionExample {

    GarbageCollectionExample ref;// = new GarbageCollection();

    public void finalize() {
        System.out.println("object is garbage collected");
    }

    public void f() {
        String s = "sdf";
        Integer i = 2;
        GarbageCollectionExample stemp = new GarbageCollectionExample();
    }

    public static void main(String args[]) {
        GarbageCollectionExample s1 = new GarbageCollectionExample();
        GarbageCollectionExample s2 = new GarbageCollectionExample();

        s1.f(); // here

        s1 = s2;
        s2 = null; //here
        System.gc();
    }
}



/*
C++ code example:
https://journal.stuffwithstuff.com/2013/12/08/babys-first-garbage-collector/

void markAll(VM* vm)
{
  for (int i = 0; i < vm->stackSize; i++) {
    mark(vm->stack[i]);
  }
}

void mark(Object* object) {
  If already marked, we're done. Check this first
  to avoid recursing on cycles in the object graph.
  if (object->marked) return;

          object->marked = 1;

          if (object->type == OBJ_PAIR) {
          mark(object->head);
          mark(object->tail);
          }
  }

void sweep(VM* vm)
{
  Object** object = &vm->firstObject;
  while (*object) {
    if (!(*object)->marked) {
      // This object wasn't reached, so remove it from the list
     //    and free it.
      Object* unreached = *object;

              *object = unreached->next;
              free(unreached);
              } else {
      // This object was reached, so unmark it (for the next GC)
      //   and move on to the next.
            (*object)->marked = 0;
            object = &(*object)->next;
 */


/* another explanation:
https://medium.com/@animeshgaitonde/garbage-collection-algorithm-mark-sweep-ed874272702d

*/

