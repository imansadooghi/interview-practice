package concept;

import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Stack;

public class InMemoryDatabaseBetter {

    public enum Function {
        set,
        delete,
        begin
    }

    public class Operation {
        Function function;
        String key;
        Integer value;

        Operation(Function function, String key, Integer value) {
            this.key = key;
            this.value = value;
            this.function = function;
        }

        private void run(){
            if (function == Function.set){
                set(key,value);
            } else if (function == Function.delete){
                delete(key);
            } else {
                System.err.println(String.format("function %s is not operable", function));
            }
        }
    }

    private Map<String,Integer> map;
    private Map<Integer,Integer> countMap;
    private Stack<Operation> rollbackStack;
    boolean rollbackMode;
    public InMemoryDatabaseBetter() {
        this.map = new HashMap<>();
        this.countMap = new HashMap<>();
        rollbackStack = new Stack<>();
        rollbackMode = false;
    }

    public Integer get(String k){
        if (map.containsKey(k))
            return map.get(k);
        return null;
    }

    public Integer getCount(int n){
       return countMap.getOrDefault(n,0);
    }

    public void set(String k, int v){
        Integer oldVal = map.put(k,v);
        countMap.put(v, countMap.getOrDefault(v,0)+1);

        if (oldVal != null) {
            countMap.put(oldVal, countMap.getOrDefault(oldVal,0)-1);
            if (!rollbackMode && !rollbackStack.isEmpty())
                rollbackStack.add(new Operation(Function.set, k, oldVal));
        } else if (!rollbackMode && !rollbackStack.isEmpty()) {
            rollbackStack.add(new Operation(Function.delete, k, null));
        }
    }

    public void delete(String k){
        Integer oldVal = map.remove(k);
        if (oldVal != null){
            countMap.put(oldVal, countMap.getOrDefault(oldVal,0)-1);
            if (!rollbackMode && !rollbackStack.isEmpty())
                rollbackStack.add(new Operation(Function.set, k, oldVal));
        } else {
            System.err.println(String.format("key %s did not exist", k));
        }
    }

    public void snapshot(){
        System.out.println("DB:");
        map.entrySet().forEach(System.out::println);
        System.out.println("counts:");
        countMap.entrySet().forEach(System.out::println);
        System.out.println("Rollback Stack:");
        rollbackStack.forEach(o -> System.out.println(o.function + " "+ o.key));
    }

    public void rollback(){
        if (rollbackStack.isEmpty()) {
            System.err.println("no transactions to roll back");
            return;
        }
        rollbackMode = true;
        while (rollbackStack.peek().function != Function.begin){
            rollbackStack.pop().run();
        }
        rollbackStack.pop(); // pop begin
        rollbackMode = false;
    }

    public void begin(){
        rollbackStack.add(new Operation(Function.begin,null,null));
    }


    private void methodInvoke(String[] tokens) {
        if (tokens.length == 0) {
            System.err.println("no command was entered");
            return;
        }

        String command = tokens[0];

        switch (command.toLowerCase()){
            case "get":
                invokeGet(tokens);
                break;
            case "count":
                invokeGetCount(tokens);
                break;
            case "set":
                invokeSet(tokens);
                break;
            case "delete":
                invokeDelete(tokens);
                break;
            case "begin":
                this.begin();
                break;
            case "rollback":
                this.rollback();
                break;
            case "snapshot":
                this.snapshot();
                break;
            default:
                System.err.println("invalid command");
        }
    }

    private void invokeGet(String[] tokens) {
        if (tokens.length != 2) {
            System.err.println("not enough arguments");
            return;
        }

        Integer value = this.get(tokens[1]);

        if (value == null)
            System.err.println(String.format("key %s does not exist", tokens[1]));
        else
            System.out.println(value);
    }

    private void invokeGetCount(String[] tokens) {
        if (tokens.length != 2) {
            System.err.println("not enough arguments");
            return;
        }

        try {
            System.out.println(this.getCount(Integer.valueOf(tokens[1])));
        } catch (NumberFormatException e ){
            System.err.println("2nd argument has to be an integer");
            return;
        }

    }

    private void invokeSet(String[] tokens) {
        if (tokens.length != 3) {
            System.err.println("not enough arguments");
            return;
        }

        try {
            set(tokens[1],Integer.valueOf(tokens[2]));
        } catch (NumberFormatException e) {
            System.err.println("3rd argument has to be an integer");
            return;
        }
    }

    private void invokeDelete(String[] tokens) {
        if (tokens.length != 2) {
            System.err.println("not enough arguments");
            return;
        }
        delete(tokens[1]);
    }

    public static void main(String[] args) {
        System.out.println("type line separated commands. to exit, type exit");

        Scanner scanner = new Scanner(System.in);

        InMemoryDatabaseBetter db = new InMemoryDatabaseBetter();
        while(true){
            if (scanner.hasNextLine()) {
                String command = scanner.nextLine();

                if (command.equalsIgnoreCase("exit")) {
                    System.exit(0);
                }
                String[] split = command.split(" ");
                db.methodInvoke(split);
            }
        }
    }

    @Test
    public void test1(){
        InMemoryDatabaseBetter db = new InMemoryDatabaseBetter();
        db.begin();
        db.set("t",2);
        db.set("r",2);
        assert db.get("t") == 2;
        assert db.getCount(2) == 2;
        db.rollback();
        assert db.get("t") == null;
        assert db.getCount(2) == 0;
    }

    public void test2(){
        String[] calls = {"set x 2","get x", "begin", "set a 2", "rollback", "count 1", "count 2"};

        InMemoryDatabaseBetter db = new InMemoryDatabaseBetter();

        for (int i = 0; i < calls.length; i++) {
            db.methodInvoke(calls[i].split(" "));
        }

    }
}
