package concept;

import javax.xml.bind.DatatypeConverter;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

public class Checksum {

    /*
idea:  LongURL --> assigned id, using counter (id++)
         or    \-> random id   (uuid)
               \-> checksum(hash)  (sha-256)     -> look at method hashEncode(String) below. the encoded result could be
                                                    cut from beginning into desired size i.e encoded.substring(6)

    how does it work?

        String content  -> take checksum(content) -> that is a byte array byte[] -> so this simply is a base 8 number

        now you can encode b64 this bytes-array that is a base 8 number.

        now having an b64 encoded string, you can serve the first n chars of it

    process:
        encode: longUrl -> id (one of above)  -> base 62 shortUrl
        decode: shortUrl base62  -> id(b10) -> longUrl via map<id,longUrl>


your confusion was:  how do we get from String to an integer within the range of  1 - 62^6 (assuming 6 chars)
                    answer is above. 3 options.

why reverse after encode? 3610 ->that 10:a forms the least valuable (rightmost) of the number. so if you dont reverse, then on decode, you should not walk from left to right. the left-most will be the least valuable if you dont reverse.

the better way to design this is here:
https://www.educative.io/courses/grokking-the-system-design-interview/m2ygV4E81AR

longUrl --SHA256-> 128
*/

    public static String checksum(String s) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");

        byte[] bytes = md.digest(s.getBytes("UTF-8"));

        // two waysBT to turn bytes from digest into an actual hexadecimal string
//        StringBuilder sb = new StringBuilder();
//        for (byte b : bytes){
//            sb.append(String.format("%02X", b&0xff));
//        }
        //System.out.println(sb);

        String hexBinary = DatatypeConverter.printHexBinary(bytes);

        System.out.println(hexBinary);

        // this line is kind of nonesense. the HexaDecimal String is the resulting value of checksum.
        // System.out.println("checksum's hash:"+hexBinary.hashCode()+" basic input hash:"+s.hashCode());

        return hexBinary;

    }

    // uses SHA-256 to generate -> byteArray of hash, then encodes it to b64 URL style:
    public static String hashEncode(String s) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");

        byte[] bytes = md.digest(s.getBytes("UTF-8"));

        Base64.Encoder encoder = Base64.getUrlEncoder();
        String encoded = encoder.encodeToString(bytes);
        System.out.println(encoded);

        return encoded;

    }

    public static void main(String[] args) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        System.out.println("checksum results:");
        checksum("aaab2");
        checksum("aaab3");
        checksum("aaab");
        checksum("aabbc");
        checksum("");
        checksum("a");
        checksum("xcvsdfasdfafffffffffffffffffffffffffffdfgdlfgdfgdfgd" +
                "fgdfgdfgdfgdgdfgdfgdfgdfgdfgdfgdfgdfgdfgdfsdfasdsafdsxcvsdfasdfaffffffffffff" +
                "fffffffffffffffdfgdlfgdfgdfgdfgdfgdfgdfgdgdfgdfgdfgdfgdfgdfgdfgdfgdfgdfsdfasds" +
                "afdsxcvsdfasdfafffffffffffffffffffffffffffdfgdlfgdfgdfgdfgdfgdfgdfgdgdfgdfgdfgdfg" +
                "dfgdfgdfgdfgdfgdfsdfasdsafdsxcvsdfasdfafffffffffffffffffffffffffffdfgdlfgdfgdfgdf" +
                "gdfgdfgdfgdgdfgdfgdfgdfgdfgdfgdfgdfgdfgdfsdfasdsafds");

        System.out.println("hash encode results:");
        hashEncode("aaab2");
        hashEncode("aaab3");
        hashEncode("aaab");
        hashEncode("aabbc");
        hashEncode("");
        hashEncode("a");
        hashEncode("xcvsdfasdfafffffffffffffffffffffffffffdfgdlfgdfgdfgd" +
                "fgdfgdfgdfgdgdfgdfgdfgdfgdfgdfgdfgdfgdfgdfsdfasdsafdsxcvsdfasdfaffffffffffff" +
                "fffffffffffffffdfgdlfgdfgdfgdfgdfgdfgdfgdgdfgdfgdfgdfgdfgdfgdfgdfgdfgdfsdfasds" +
                "afdsxcvsdfasdfafffffffffffffffffffffffffffdfgdlfgdfgdfgdfgdfgdfgdfgdgdfgdfgdfgdfg" +
                "dfgdfgdfgdfgdfgdfsdfasdsafdsxcvsdfasdfafffffffffffffffffffffffffffdfgdlfgdfgdfgdf" +
                "gdfgdfgdfgdgdfgdfgdfgdfgdfgdfgdfgdfgdfgdfsdfasdsafds");
    }

}




