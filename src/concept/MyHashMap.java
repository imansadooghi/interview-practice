package concept;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/*
*  Iman Sadooghi
*
*  HashMap implementation practice
*
* */
public class MyHashMap <K,V> { // Point 1: <K,V> generics

    // TODO: change this to array instead! Node[] buckets
    private List<Node> buckets;
    private int size, capacity;
    private final float loadFactor;

    private static final int DEFAULT_INITIAL_CAPACITY = 1000;
    private static final float DEFAULT_LOAD_FACTOR = 0.75f;
    private static final float DEFAULT_CAPACITY_MULTIPLIER = 2f;

    public MyHashMap(int initialCapacity, float loadFactor){
        size = 0;
        capacity = initialCapacity;
        this.loadFactor = loadFactor;

        buckets = new ArrayList<Node>(capacity);
        // Point **: DONT FORGET TO make empty BUCKETS
        for (int i = 0; i < capacity; i++)
            buckets.add(null);

    }

    public MyHashMap(int initialCapacity){
        this(initialCapacity,DEFAULT_LOAD_FACTOR);
    }

    public MyHashMap(){
        this(DEFAULT_INITIAL_CAPACITY,DEFAULT_LOAD_FACTOR);
    }

    public V get(K key){
        if (key == null)
            return null;

        int index = getBucketIndex(key);
        Node node = getNode(buckets.get(index), key);
        return (node == null) ? null : node.getValue();
    }

    /*
    * IMPORTANT POINT: first, I was thinking I should add stuff to end of chain!
    *   NO! it is easier to add as head. and have it point to old head!
    * */
    public V put(K key, V value){
        int index = getBucketIndex(key);
        Node node = buckets.get(index);
        Node firstNode = node;

        // whole bucket doesnt exists
        if (node == null){
            node = new Node(key,value);
            buckets.set(index,node);
        } else {
            // if already exists: replace
            while (node != null) {
                if (node.getKey().equals(key)) {
                    V oldVal = node.getValue();
                    node.setValue(value);
                    return oldVal;
                }
                node = node.next;
            }

            // doesnt exist (bucket exists but this certain key doesnt)
            node = new Node(key, value);
            node.setNext(firstNode);
            buckets.set(index, node);
        }

        size++;
        if ((float)size/capacity > loadFactor)
            resize();

        return null;
    }

    public int size() {
        return size;
    }

    public boolean containsKey(K key){
        Node node = getNode(buckets.get(getBucketIndex(key)),key);
        return node != null;
    }

    public V remove(Object key){
        int index = getBucketIndex(key);
        Node firstNode = buckets.get(index);

        if (firstNode == null)
            return null;

        // POINT: I had a bug: if first node is our node, dont set that whole bucket index to null. set it to the nextNode
        if (firstNode.getKey().equals(key)){
            V oldVal = firstNode.getValue();
            buckets.set(index,firstNode.getNext());
            size--;
            return oldVal;
        }

        Node prev = firstNode;
        Node cur = firstNode.getNext();
        while (cur != null){
            if (cur.getKey().equals(key)){
                V oldVal = cur.getValue();
                prev.setNext(cur.getNext());
                size--;
                return oldVal;
            }
            prev = cur;
            cur = cur.getNext();
        }
        return null;
    }

    //point 2: how to find the index!
    private int getBucketIndex(Object key){
        return (key == null) ? 0 : key.hashCode() % capacity;
    }

    private Node getNode(Node node, K key){
        while ( node != null){
            if (node.getKey().equals(key))
                return node;
            node = node.getNext();
        }
        return null;
    }

    private void resize(){
        capacity *= DEFAULT_CAPACITY_MULTIPLIER;

        List<Node> oldBuckets = buckets;
        buckets = new ArrayList<>(capacity);
        for (int i = 0; i < capacity; i++)
            buckets.add(null);

        for (Node node : oldBuckets)
            while(node != null) {
                this.put(node.getKey(), node.getValue());
                node = node.next;
            }
        System.out.println("resized to:" + buckets.size());
        System.out.println(buckets);
    }

    private class Node {
        K key;
        V value;

        Node next;

        public Node(K key, V value) {
            this.key = key;
            this.value = value;
        }

        public final String toString() { return key + "=" + value; }

        public K getKey() {
            return key;
        }

        public void setKey(K key) {
            this.key = key;
        }

        public V getValue() {
            return value;
        }

        public void setValue(V value) {
            this.value = value;
        }

        public Node getNext() {
            return next;
        }

        public void setNext(Node next) {
            this.next = next;
        }
    }
}

