package concept;

public class Knapsack {
    int[][] mem;
    public int knapsack(int[] wt, int[] val, int weightLimit){

        //return ks(wt,val,weightLimit,0);

        mem = new int[wt.length+1][weightLimit+1];
        return knapsackD(wt,val,weightLimit,0);
        //return knapsackD2(wt,val,weightLimit,wt.length);
    }

    /* tot: total value so far.  wll: weight limit left
                                                    include                           Vs             Exclude
    *  define: opt knapsack at iNew =    ks of (prev index, wll-weight[iNew-1]) + val[iNew-1]  Vs  ks of (prev index, wll)
    *  look at previous step
    *
    *  base cases:  if  wll == 0 --> no weight left. full bag or you can think. if zero capacity bag --> 0
    *            or if  iNew == 0  --> no item (since always looking at one before. hence item -1th)    --> 0
    *
    *            or if wll <  weight[iNew-1]  --> need only keep exclude.   skip inclusion of this item at iNew-1
    */
    public int ks2(int[] wt, int[] val, int wll, int i) {

        if (i == 0 || wll == 0)
            return 0;

        if ( wll < wt[i-1])
            return ks2(wt,val,wll,i-1);

        int ksInc = ks2(wt,val,wll-wt[i-1],i-1) + val[i-1];
        int ksX = ks2(wt,val,wll,i-1);

        return Math.max(ksInc, ksX);
    }

    /*
    *  same as above, but starts from 0. looking forward.
    *  define: ks of iNew is:   ks (of include iNew, wll - wt[iNew]) + val[iNew]  Vs  ks (of exclude iNew, wll)
    *
    *  base case:    wll is 0   or   we are at the end. iNew==length
    * */
    public int ks(int[] wt, int[] val, int wll, int i) {

        if (i == wt.length || wll == 0)
            return 0;

        if ( wll < wt[i])
            return ks(wt,val,wll,i+1);

        int ksInc = ks(wt,val,wll-wt[i],i+1) + val[i];
        int ksX = ks(wt,val,wll,i+1);

        return Math.max(ksInc, ksX);
    }

    /** top-down dynamic programming
     *
     * @param wt
     * @param val
     * @param weightLimit
     * @param i
     * @return
     *
     * approach: same as ks   --> starts from 0;     This was CLOSER to my thoughts!
     */
    public int knapsackD(int[] wt, int[] val, int weightLimit, int i) {
        if (mem[i][weightLimit] != 0)
            return mem[i][weightLimit];

        if (i == wt.length || weightLimit == 0 )
            return 0;

        int ex =  knapsackD(wt,val,weightLimit, i+1);

        if (weightLimit < wt[i])
            mem[i][weightLimit] = ex;
        else {
            int inc = knapsackD(wt, val, weightLimit - wt[i], i + 1) + val[i];
            mem[i][weightLimit] = Math.max(inc, ex);
        }
        return mem[i][weightLimit];
    }

    // top-down dynamic programming
    public int knapsackD2(int[] wt, int[] val, int weightLimit, int i) {
        if (mem[i][weightLimit] != 0)
            return mem[i][weightLimit];

        // important: i==0 -> i is always one ahead. so at index 0, knapsack is empty.      also if weightLimit is 0, you cant add anything!
        if (i == 0 || weightLimit == 0)
            return 0;

        int knExc =  knapsackD(wt,val,weightLimit, i-1);

        // if weight of package at i-1 is already more than left limit, you can only consider NOT Including it.
        if (weightLimit < wt[i-1])
            mem[i][weightLimit] = knExc;
        else {
            int knInc = knapsackD2(wt, val, weightLimit - wt[i-1], i -1) + val[i-1];
            mem[i][weightLimit] = Math.max(knInc, knExc);
        }
        return mem[i][weightLimit];
    }

//    public int knapsack(int[] wt, int[] val, int weightLimit) {
//        int items = wt.length;
//        int[][] mem = new int[items+1][weightLimit+1];
//
//        for (int iNew = 1; iNew <= items; iNew++) {
//            for (int jNew=1; jNew<= weightLimit; jNew++){
//                mem[iNew][jNew] = Math.max(mem[iNew-1][jNew] + val[iNew], mem[iNew-1][jNew]);
//            }
//        }
//    }

        // Returns the maximum value that can be put in a knapsack of capacity W
    static int knapSack(int W, int wt[], int val[], int n)
    {
        // Base Case
        if (n == 0 || W == 0)
            return 0;

        // If weight of the nth item is more than concept.Knapsack capacity W, then
        // this item cannot be included in the optimal solution
        if (wt[n-1] > W)
            return knapSack(W, wt, val, n-1);

            // Return the maximum of two cases:
            // (1) nth item included
            // (2) not included
        else return Math.max( val[n-1] + knapSack(W-wt[n-1], wt, val, n-1),
                knapSack(W, wt, val, n-1)
        );
    }

    public static void main(String[] args) {
        int val[] = {40, 10, 10, 20, 10, 113, 50, 32};
        int wt[] = {  1,  2,  3,  1,  3,   6,  4,  3};
        int wl = 6;

//        int val[] = new int[]{60, 100, 120};
//        int wt[] = new int[]{10, 20, 30};
//        int  wl = 50;
        int n = val.length;

        Knapsack kn = new Knapsack();

        System.out.println( kn.knapsack(wt,val,wl));
        System.out.println( knapSack(wl,wt,val,n));

    }
}

