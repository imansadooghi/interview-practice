package concept;

import java.util.ArrayList;
import java.util.List;

/*
*  Iman Sadooghi
*
*  HashMap implementation practice
*
* */
public class MyHashMap2<K,V> { // Point 1: <K,V> generics

    // TODO: change this to array instead! Node[] buckets
    private Node[] buckets;
    private int size, capacity;
    private final float loadFactor;

    private static final int DEFAULT_INITIAL_CAPACITY = 1000;
    private static final float DEFAULT_LOAD_FACTOR = 0.75f;
    private static final float DEFAULT_CAPACITY_MULTIPLIER = 2f;

    public MyHashMap2(int initialCapacity, float loadFactor){
        size = 0;
        capacity = initialCapacity;
        this.loadFactor = loadFactor;

        buckets = new Node[capacity];

    }

    public MyHashMap2(int initialCapacity){
        this(initialCapacity,DEFAULT_LOAD_FACTOR);
    }

    public MyHashMap2(){
        this(DEFAULT_INITIAL_CAPACITY,DEFAULT_LOAD_FACTOR);
    }

    public Object get(K key){
        if (key == null)
            return null;

        int index = getBucketIndex(key);
        Node prev = find(buckets[index], key);
        if (prev.next == null)
            return null;

        return prev.next.getValue();
    }

    /*
    * IMPORTANT POINT: first, I was thinking I should add stuff to end of chain!
    *   NO! it is easier to add as head. and have it point to old head!
    * */
    public void put(K key, V value){
        int index = getBucketIndex(key);
        if (buckets[index] == null)
            buckets[index] = new Node(null, null);

        Node prev = find(buckets[index],key);
        if (prev.next != null)
            prev.next.setValue(value);
        else{
            prev.next = new Node(key,value);
            size++;
            if ((float)size/capacity > loadFactor)
                resize();
        }
    }

    public int size() {
        return size;
    }

    public boolean containsKey(K key){
        Node prev = find(buckets[getBucketIndex(key)],key);
        return prev.next != null;
    }

    public void remove(K key){
        int index = getBucketIndex(key);
        Node firstNode = buckets[index];

        if (firstNode == null)
            return;

        Node prev = find(firstNode,key);
        if (prev.next != null)
            prev.next = prev.next.next;
    }

    private Node find(Node head, K key){
        Node cur = head, prev = null;
        while(cur != null && cur.key != key){
            prev = cur;
            cur = cur.next;
        }

        return prev;
    }
    //point 2: how to find the index!
    private int getBucketIndex(Object key){
        return (key == null) ? 0 : key.hashCode() % capacity;
    }

    private Node getNode(Node node, K key){
        while ( node != null){
            if (node.getKey().equals(key))
                return node;
            node = node.getNext();
        }
        return null;
    }

    private void resize(){
        capacity *= DEFAULT_CAPACITY_MULTIPLIER;

        Node[] oldBuckets = buckets;
        buckets = new Node[capacity];

        for (Node node : oldBuckets){
            if (node != null)
                node = node.next;
            while(node != null) {
                this.put((K)node.getKey(), (V)node.getValue());
                node = node.next;
            }
        }
        System.out.println(buckets);
    }

    public static void main(String[] args) {
        MyHashMap2<String,Integer> map = new MyHashMap2<>();
        for (int i=0;i<10;i++)
            map.put(String.valueOf(i),i+1);

        System.out.println(map.get(""));
    }
}


class Node<K,V> {
    K key;
    V value;

    Node next;

    public Node(K key, V value) {
        this.key = key;
        this.value = value;
    }

    public final String toString() { return key + "=" + value; }

    public K getKey() {
        return key;
    }

    public void setKey(K key) {
        this.key = key;
    }

    public V getValue() {
        return value;
    }

    public void setValue(V value) {
        this.value = value;
    }

    public Node getNext() {
        return next;
    }

    public void setNext(Node next) {
        this.next = next;
    }
}

