package concept;

import java.util.*;
public class LeakyBucketExample {

    interface LeakyBucket {
        //boolean shouldConsumeRequest();
        boolean shouldConsumeRequest(String resource, String ip);
    }

    class Info {
        long currentSize;
        long lastTs;
    }
    static class ConcreteLeakyBucket implements LeakyBucket{
        private final long maxBucketSize;
        private final long leakRate;
        private Map<String,Info> map;
        private long currentSize;
        private long lastTs;

        public ConcreteLeakyBucket (long maxSize, long rate){
            this.maxBucketSize = maxSize;
            this.leakRate = rate;
            map = new HashMap();

            currentSize = 0;
            lastTs = System.nanoTime();
        }

        public boolean shouldConsumeRequest(String resource, String ip){
            Info info = map.getOrDefault(ip+":"+resource,new Info());
            leak(info);
            map.put(ip+":"+resource,info);
      ,,
            if (info.currentSize < maxBucketSize ){
                info.currentSize++;

                return true;
            }

            return false;
        }

        private void leak(Info info){
            long now = System.nanoTime();

            long leakCount = (long)((now - info.lastTs) * leakRate / 1e9);
            if (leakCount>0)
                System.out.println("leaking:"+leakCount);
            info.currentSize = (info.currentSize > leakCount) ? info.currentSize -  leakCount : 0;

            info.lastTs = now;
        }
    }

    public static void main(String[] args) throws Exception {
        LeakyBucket lb = new ConcreteLeakyBucket(10,1);

        for (int i=0;i<15;i++){
            System.out.println(i);
            System.out.println(lb.shouldConsumeRequest());


        }

        Thread.sleep(1000);
        System.out.println(lb.shouldConsumeRequest());
    }
}
