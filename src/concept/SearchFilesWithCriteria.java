package concept;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

public class SearchFilesWithCriteria {
    List<File> matchedList;

    public SearchFilesWithCriteria() {
        matchedList = new ArrayList<>();
    }



    public List<File> searchWithCriteria(File path, Matcher matcher){
        if (path == null)
            return null;
        searchRec(path,matcher);
        return matchedList;
    }

    private void searchRec(File file, Matcher matcher){
        if (!file.isDirectory()){
            if (matcher.matches(file))
                matchedList.add(file);
            return;
        }
        File[] children = file.listFiles();

        for (File f : children){
            if (f.isDirectory())
                searchRec(f,matcher);
            else if (matcher.matches(f))
                matchedList.add(f);
        }

    }

    // it could be also written with java Predicate
    private void searchRecPredicate(File file,Predicate<File> matcher){
        if (!file.isDirectory()){
            if (matcher.test(file))
                matchedList.add(file);
            return;
        }
        File[] children = file.listFiles();

        for (File f : children){
            if (f.isDirectory())
                searchRecPredicate(f,matcher);
            else if (matcher.test(f))
                matchedList.add(f);
        }

    }

    public interface Matcher {
        public boolean matches(File file);
    }

    public static void main(String[] args) {
        // format and size matcher:  .java   &&  size > 4000
        File path = new File("/Users/imansadooghi/IdeaProjects/interview-practice");
        SearchFilesWithCriteria fileSearch = new SearchFilesWithCriteria();

        // with lambda
        fileSearch.searchRec(path, file -> { return file.isFile() && file.getName().endsWith(".java") && file.length() > 4000;} );

/*      OLD JAVA way
        fileSearch.searchRec(path, new Matcher() {
            @Override
            public boolean matches(File file) {
                return file.isFile() && file.getName().endsWith(".java") && file.length() > 4000;
            }
        });
*/

System.out.println(fileSearch.matchedList);
    }
}