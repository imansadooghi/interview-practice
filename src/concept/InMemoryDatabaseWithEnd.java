package concept;

import java.util.*;

public class InMemoryDatabaseWithEnd {

    public enum Function {
        set,
        delete,
        begin,
        end
    }

    public class Operation {
        Function function;
        String key;
        Integer value;

        Operation(Function function, String key, Integer value) {
            this.key = key;
            this.value = value;
            this.function = function;
        }

        private void run(){
            if (function == Function.set){
                set(key,value);
            } else if (function == Function.delete){
                delete(key);
            } else {
                System.err.println(String.format("function %s is not operable", function));
            }
        }
    }

    private Map<String,Integer> map;
    private Stack<Operation> rollbackStack;
    private int beginCount, endCount;

    public InMemoryDatabaseWithEnd() {
        this.map = new HashMap<>();
        rollbackStack = new Stack<>();
        beginCount = 0;
        endCount = 0;
    }

    public Integer get(String k){
        if (map.containsKey(k))
            return map.get(k);
        return null;
    }

    public Integer set(String k, int v){
        return map.put(k,v);
    }

    public Integer delete(String k){
        return  map.remove(k);
    }

    public void snapshot(){
        System.out.println("DB:");
        map.entrySet().forEach(System.out::println);
        System.out.println("Rollback Stack:");
        rollbackStack.forEach(o -> System.out.println(o.function + " "+ o.key));
    }

    public void rollback(){
        if (rollbackStack.isEmpty()) {
            System.err.println("no rollbackStack left to roll back");
            return;
        }

        if ( rollbackStack.peek().function != Function.end){ // single operation function: set or delete
            rollbackStack.pop().run();
        } else { // multi-operation function
            rollbackStack.pop(); // pop end
            endCount--;
            while (rollbackStack.peek().function != Function.begin){
                rollback();
            }
            rollbackStack.pop(); // pop begin
            beginCount--;
        }
    }

    public void begin(){
        rollbackStack.add(new Operation(Function.begin,null,null));
        beginCount++;
    }

    public void end(){
        if (endCount == beginCount)
            System.err.println("no more transactions left to end!");
        else {
            rollbackStack.add(new Operation(Function.end,null,null));
            endCount++;
        }
    }


    private void methodInvoke(String[] tokens) {
        if (tokens.length == 0) {
            System.err.println("no command was entered");
            return;
        }

        String command = tokens[0];

        switch (command.toLowerCase()){
            case "get":
                invokeGet(tokens);
                break;
            case "set":
                invokeSet(tokens);
                break;
            case "delete":
                invokeDelete(tokens);
                break;
            case "begin":
                this.begin();
                break;
            case "end":
                this.end();
                break;
            case "rollback":
                this.rollback();
                break;
            case "snapshot":
                this.snapshot();
                break;
            default:
                System.err.println("invalid command");
        }
    }

    private void invokeGet(String[] tokens) {
        if (tokens.length != 2) {
            System.err.println("not enough arguments");
            return;
        }

        Integer value = this.get(tokens[1]);

        if (value == null)
            System.err.println(String.format("key %s does not exist", tokens[1]));
        else
            System.out.println(value);
    }

    private void invokeSet(String[] tokens) {
        if (tokens.length != 3) {
            System.err.println("not enough arguments");
            return;
        }

        try {
            Integer oldVal = set(tokens[1],Integer.valueOf(tokens[2]));
            // add operation log
            if (oldVal != null) {
                rollbackStack.add(new Operation(Function.set, tokens[1], oldVal));
            } else {
                rollbackStack.add(new Operation(Function.delete, tokens[1], null));
            }
        } catch (NumberFormatException e) {
            System.err.println("3rd argument has to be an integer");
            return;
        }
    }

    private void invokeDelete(String[] tokens) {
        if (tokens.length != 2) {
            System.err.println("not enough arguments");
            return;
        }

        Integer oldVal = delete(tokens[1]);
        if (oldVal != null){
            rollbackStack.add(new Operation(Function.set, tokens[1], oldVal));
        } else {
            System.err.println(String.format("key %s did not exist", tokens[1]));
        }
    }

    public static void main(String[] args) {
        System.out.println("type line separated commands. to exit, type exit");

        Scanner scanner = new Scanner(System.in);

        InMemoryDatabaseWithEnd db = new InMemoryDatabaseWithEnd();
        while(true){
            if (scanner.hasNextLine()) {
                String command = scanner.nextLine();

                if (command.equalsIgnoreCase("exit")) {
                    System.exit(0);
                }
                String[] split = command.split(" ");
                db.methodInvoke(split);
            }
        }
    }
}
