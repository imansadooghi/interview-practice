package concept;

import java.io.*;
import java.util.*;


/*
*  given a top Directory with files (nested) that have lines with only numbers  ->  return top k of all numbers
rootDir\
   subDir\
     file4
     file5
     ...
       subDir2\
         file6
         file7
         ...
  ...
  file1
  file2
  file3
  ...

    - have a MinHeap PQ that keeps top k
    - recursively go through each file
    - for each number in file:
        - if pq size < k  -> add number
        - else if pq head is smaller than number -> replace head with number


n: number of files
l: lines of file of each file
time complexity -> O(total(l) * logk) -> goes through all lines. worst case -> update pq each time
space complexity -> O(n) for file Objects (names)    BufferedReader keeps only 1 line at a time? (TODO:IDK the answer)
                                                     -> if so =>
                                                                    O(n) for n File List (not actual files, just n objects)
                                                                    O(1) since bufferedReader reads line by line
                                                                    O(k) for pq
                                                                    => O(n)

*
* */
public class TopKNumFinder {

    void getFiles(File root, List<File> res) {

        for (File child : root.listFiles()) {
            if (child.isDirectory())
                getFiles(child, res);
            else
                res.add(child);
        }

    }

    public int[] getTopKLargestIntegers(String rootDir, int k) throws IOException {
        File root = new File(rootDir);
        List<File> res = new ArrayList<>();
        getFiles(root, res);

        Queue<Integer> pq = new PriorityQueue();

        BufferedReader reader;
        for (File f : res) {
            reader = new BufferedReader(new FileReader(f));
            while (true) {
                String num = reader.readLine();
                int i = 0;
                try {
                    i = Integer.parseInt(num);
                } catch (Exception e) {
                    System.out.println(e);
                    break;
                }
                // update pq logic
                if (pq.size() < k)
                    pq.add(i);
                else if (pq.size() == k) {
                    if (pq.peek() < i) {
                        pq.poll();
                        pq.add(i);
                    }
                }
            }

        }

        int[] ans = new int[k];
        for (int i = 0; i < k; i++)
            ans[i] = pq.poll();

        return ans;
    }


    public static void main(String[] args) {
        TopKNumFinder tk = new TopKNumFinder();
        try {
            System.out.println(Arrays.toString(tk.getTopKLargestIntegers("/Users/imansadooghi/IdeaProjects/interview-practice/topk", 7)));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
