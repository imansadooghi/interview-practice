package concept;

import java.util.LinkedList;
import java.util.PriorityQueue;
import java.util.Queue;

public class HuffmanCoding {

    /*
    *   Create a PriorityQueue of Nodes  -> MinHeap
    *
    *   While Queue size > 1
    *       keep getting two nodes at a time.
    *       left bigger than right
    *
    *       Merge them and Create Parent
    *
    *       Connect each to new parent.
    *
    *       push parent back to Queue
    *
    *   at End PQ has 1 Node which is root!
    *
    * */
    public Node encode(char[] chars, int[] freqs){
        PriorityQueue<Node> pq = new PriorityQueue<>((Node a, Node b) -> a.freq - b.freq);

        for (int i=0; i<chars.length;i++)
            pq.add( new Node(String.valueOf(chars[i]),freqs[i]));

        while (pq.size() > 1){
            Node left = pq.poll();
            Node right = pq.poll();

            Node parent = new Node(left.word+right.word,left.freq+right.freq, left, right);

            left.setParent(parent);
            right.setParent(parent);

            pq.add(parent);
        }
        //System.out.println(pq.peek());
        return pq.poll();
    }

    public void printCodes(Node root){
        printCodes(root,"");
    }
    private void printCodes(Node root, String str){
        if (root == null)
            return;
        if (root.left == null && root.right == null)
            System.out.println(root.word + ":" + str);

        if (root.left != null)
            printCodes(root.left, str+"0");
        if (root.right != null)
            printCodes(root.right, str+"1");
    }

    public static void main(String[] args){
        char[] chars = {'a','b','c','d','r','q','j','z'};
        int[]  freqs = { 14,  7,  6, 11,  9,  2,  3, 2};
//        char[] chars = { 'a', 'b', 'c', 'd', 'e', 'f' };
//        int[] freqs = { 5, 9, 12, 13, 16, 45 };

        HuffmanCoding hc = new HuffmanCoding();
        Node encoded = hc.encode(chars,freqs);

        hc.printCodes(encoded);
    }


    class Node{
        int freq;
        String word;

        Node parent;
        Node left;
        Node right;

        public int getFreq() {
            return freq;
        }

        public void setFreq(int freq) {
            this.freq = freq;
        }

        public String getWord() {
            return word;
        }

        public void setWord(String word) {
            this.word = word;
        }

        public Node getParent() {
            return parent;
        }

        public void setParent(Node parent) {
            this.parent = parent;
        }

        public Node getLeft() {
            return left;
        }

        public void setLeft(Node left) {
            this.left = left;
        }

        public Node getRight() {
            return right;
        }

        public void setRight(Node right) {
            this.right = right;
        }


        public Node(String word, int freq, Node left, Node right) {
            this.freq = freq;
            this.word = word;
            this.left = left;
            this.right = right;
        }

        public Node(String word, int freq) {
            this.freq = freq;
            this.word = word;
        }

        public String toString(){
            StringBuffer sb = new StringBuffer();
            Queue<Node> q = new LinkedList<>();
            q.add(this);

            while (!q.isEmpty()){
                int levelSize = q.size();
                for (int i=0; i<levelSize; i++){
                    Node node = q.poll();
                    if (node != null) {
                        sb.append(node.word + ":" + node.freq + " ");
                        q.add(node.left);
                        q.add(node.right);
                    }else
                        sb.append("NULL ");
                }
                sb.append(System.lineSeparator());
            }
            return sb.toString();
        }
    }
}
