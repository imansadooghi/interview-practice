import java.util.*;

public class MaxAlternatingPathSumTwoArrays {

    //    https://www.geeksforgeeks.org/maximum-sum-path-across-two-arrays/
    /* Idea:
     *   similar to merge to sorted arrays.
     *   keep adding each separately. pointer moves on the smaller number. until we find common nums. then both sa & sb are max of sa&sb
     *
     *
     *   second idea: similar walk as first.
     *   keep partial sum since last common for each. when reached common -> add the bigger one of sa and sb to smax.  also, after while is done -> smax gets the max of two again.
     * */

    public int maxPathSum(int[] a, int[] b) {
        int sa = 0, sb = 0;
        int i = 0, j = 0;

        while (i < a.length || j < b.length) {
            if (j == b.length)
                sa += a[i++];
            else if (i == a.length)
                sb += b[j++];
            else if (a[i] < b[j])
                sa += a[i++];
            else if (b[j] < a[i])
                sb += b[j++];
            else { // equal
                sa = sb = Math.max(sa, sb) + a[i]; // or b[jNew]
                i++;
                j++;
            }
        }

        return Math.max(sa, sb);
    }

    public static void main(String[] args) {
        MaxAlternatingPathSumTwoArrays m = new MaxAlternatingPathSumTwoArrays();
        int[] a = {2, 7, 10, 12, 15, 16, 17, 21, 25, 30, 34,36,46};
        int[] b = {1, 5, 7, 8, 10, 15, 16, 19, 25, 34};
        System.out.println(m.maxPathSum(a, b));
        System.out.println(m.maxPathSum2(a, b));
    }

    public int maxPathSum2(int[] a, int[] b) {
        int sa = 0, sb = 0, smax = 0;
        int i=0, j=0;
        while (i < a.length || j < b.length) {
            if (j == b.length || a[i] < b[j])
                sa += a[i++];
            else if (i == a.length || a[i] > b[j])
                sb += b[j++];
            else {
                smax += Math.max(sa,sb) + a[i];
                sa = sb = 0;
                i++; j++;
            }
        }
        smax += Math.max(sa,sb);

        return smax;
    }
}
