package lc;

public class MaxConsecutiveOnesII {
    /*

     * Description,
Given a binary array, find the maximum number of consecutive 1s in this array if you can flip at most one 0.

Example 1:
Input: [1,0,1,1,0]
Output: 4
Explanation: Flip the first zero will get the the maximum number of consecutive 1s.
After flipping, the maximum number of consecutive 1s is 4.

Note:

The input array will only contain 0 and 1.
The length of input array is a positive integer and will not exceed 10,000
Follow up:
What if the input numbers come in one by one as an infinite stream? In other words,
you can’t store all numbers coming from the stream as it’s too large to hold in memory. Could you solve it efficiently?
     */

    // this one is actually III    k= #zeros allowed to flip
    public int findMaxConsecutiveOnes(int[] nums, int k) {

        int max = 0, l = 0, zc = 0;

        for (int i=0;i<nums.length;i++){
            if (nums[i] == 0){
                zc++;
                while(zc > k ){
                    if (nums[l] == 0)
                        zc--;
                    l++;
                }
            }
            max = Math.max(max, i-l+1);
        }

        return max;
    }



    public int findMaxConsecutiveOnesFIRSTRY(int[] nums) {
        // hack: rights has one extra index, so I dont have to check the last one for this case  1111110
        // in that case, rights[last+1] = 0 -->
        int[] lefts = new int[nums.length], rights = new int[nums.length + 1];
        int max = 0, cur = 0;

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] == 1)
                lefts[i] = ++cur;
            else
                cur = 0;
        }

        for (int i = nums.length - 1; i >= 0; i--) {
            if (nums[i] == 1)
                rights[i] = ++cur;
            else
                cur = 0;
        }

        // calculate for iNew==0
        if (nums[0] == 0)
            max = rights[1];
        for (int i = 1; i < nums.length; i++) {
            if (nums[i] == 0)
                max = Math.max(max, lefts[i - 1] + rights[i + 1]);
        }
        // for corner case of all 00000000
        return (max == 0) ? 1 : max + 1;
    }

    public static void main(String[] args){
        //int[] nums = {1,1,1,0,1,1,1,0,0,1,1,1,1,0,1,1,0,1};
        int[] nums = {0,1,1,0,1,1,1,0,0,1,1,1,1,0,1,1,0,1};
        MaxConsecutiveOnesII m = new MaxConsecutiveOnesII();
        System.out.println( m.findMaxConsecutiveOnes(nums,1));
    }
}
