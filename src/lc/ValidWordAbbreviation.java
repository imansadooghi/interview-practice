package lc;

public class ValidWordAbbreviation {
    /*
    *  lc 408
Given a non-empty string s and an abbreviation abbr, return whether the string matches with the given abbreviation.
A string such as "word" contains only the following valid abbreviations:
["word", "1ord", "w1rd", "wo1d", "wor1", "2rd", "w2d", "wo2", "1o1d", "1or1", "w1r1", "1o2", "2r1", "3d", "w3", "4"]
Notice that only the above abbreviations are valid abbreviations of the string "word". Any other string is not a valid abbreviation of "word"

Note:
Assume s contains only lowercase letters and abbr contains only lowercase letters and digits.
Example 1:
Given s = "internationalization", abbr = "i12iz4n":

Return true.
Example 2:
Given s = "apple", abbr = "a2e":

Return false.
    * */

    /*
walk on both with two pointers -> while both <len.
if abbr has a number -> keep going forward until you get the whole number (in case of >=10)
                     -> skip word for that many letters.
if abbs is letter -> check if same
*/
    public static boolean isValid(String word, String abr){
        char[] w = word.toCharArray();
        char[] ab = abr.toCharArray();

        int i=0, j=0; // i for w, and j for ab
        while (j < ab.length && i<w.length){
            if (Character.isDigit(ab[j])){ // get the multi-digit number
                if (ab[j] == '0')
                    return false;
                int b = j;
                while (j < ab.length-1 && Character.isDigit(ab[j+1]))
                    j++;
                i += Integer.valueOf(abr.substring(b,++j));
            } else {
                if (w[i++] != ab[j++])
                    return false;
            }
        }
        return i==w.length && j ==ab.length;
    }

    public static void main(String[] args) {
        System.out.println(isValid("word","4"));
        System.out.println(isValid("word","2r1"));
        System.out.println(isValid("internationalization", "i12iz4n"));
        System.out.println(isValid("internationalization", "i18n"));
        System.out.println(isValid("internationalizationn", "10n10"));
        System.out.println(isValid("",""));
        System.out.println(isValid("apple","a2e"));
    }
}
