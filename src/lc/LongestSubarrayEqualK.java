package lc;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
//https://www.programcreek.com/2014/10/leetcode-maximum-size-subarray-sum-equals-k-java/
//https://www.geeksforgeeks.org/longest-sub-array-sum-k/
// Leetcode 325 (locked)
public class LongestSubarrayEqualK {
    /*
     *  Idea: if we have sum upto each index.    later, we can say   difference = currentSum - k      --> so k = sum - difference
     * */
    static int longestSubarrayEqK(int[] nums, int k) {
        int sum = 0, maxLen=0;
        // map of: sum and index     sum is sum from 0 to that index
        Map<Integer,Integer> map = new HashMap();
        map.put(0,-1);
        for (int i = 0; i < nums.length; i++) {
            sum += nums[i];
            int dif = sum - k;
            if (map.containsKey(dif))
                maxLen = Math.max(maxLen, i - map.get(dif));
            // if there was no sums before, add this.
            map.putIfAbsent(sum,i);
        }
        return maxLen;
    }

    public static void main(String[] args) {
        assertEquals(6, longestSubarrayEqK(new int[]{1,2,-3,0,0,2,2},2));
        assertEquals(1, longestSubarrayEqK(new int[]{0},0));
        assertEquals(0, longestSubarrayEqK(new int[]{1},0));
        assertEquals(4, longestSubarrayEqK(new int[]{1, -1, 5, -2, 3},3));
        System.out.println("all passed");

    }
}

