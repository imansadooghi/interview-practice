package lc;

import java.util.*;

/* idea: we calculate of distance from each building on each 0 point on grid on a separate distance-grid, and save reachability of each  point 0 in another grid.

        - go from each existing building with BFS. for each point 0 -> add dist from current building to dist. and add 1 to reach grid.

        - at end we will have total distance and total reachable building rootCount on each point 0. =. if reach is == total number of buidlings -> compare totDist with best

    time: O(bn)  where b is number of existing buildings
*/

public class ShortestDistanceFromAllBuildings {
    /*
    * Question: LC317
    * You want to build a house on an empty land which reaches all buildings in the shortest amount of distance. You can only move up, down, left and right. You are given a 2D grid of values 0, 1 or 2, where:
    * Each 0 marks an empty land which you can pass by freely.
    * Each 1 marks a building which you cannot pass through.
    * Each 2 marks an obstacle which you cannot pass through.
    * For example, given three buildings at (0,0), (0,4), (2,2), and an obstacle at (0,2):
1 - 0 - 2 - 0 - 1
|   |   |   |   |
0 - 0 - 0 - 0 - 0
|   |   |   |   |
0 - 0 - 1 - 0 - 0
    * The point (1,2) is an ideal empty land to build a house, as the total travel distance of 3+3+1=7 is minimal. So return 7.
    * Note: There will be at least one building. If it is not possible to build such house according to the above rules, return -1.
    * */
    int[][] distance;
    int[][] reach;
    public int shortestDistance(int[][] grid) {
        distance = new int[grid.length][grid[0].length];
        reach = new int[grid.length][grid[0].length];

        int numBuildings = 0;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (grid[i][j] == 1) {
                    traverse(grid, i, j);
                    numBuildings++;
                }
            }
        }

        int iloc = 0, jloc = 0;
        int bestDist = Integer.MAX_VALUE;
        for (int i = 0; i < grid.length; i++) {
            for (int j = 0; j < grid[0].length; j++) {
                if (reach[i][j] == numBuildings && distance[i][j] < bestDist) {
                    bestDist = distance[i][j];
                    iloc = i;
                    jloc = j;
                }
            }
        }

        System.out.println(String.format("i:%d j:%d",iloc,jloc));
        return bestDist;
    }

    // current building at i,j,
    public void traverse(int[][] grid, int i, int j){
        Queue<Point> q = new LinkedList<>();

        // add all 4 neighbors of building to q  ->
       // q.offer(new Point(i,j,0));
        q.offer(new Point(i+1,j,1));
        q.offer(new Point(i-1,j,1));
        q.offer(new Point(i,j+1,1));
        q.offer(new Point(i,j-1,1));

        while(!q.isEmpty()){
            Point p = q.poll();
            // out of range
            if (p.i < 0 || p.i == grid.length || p.j < 0 || p.j == grid[0].length || grid[p.i][p.j] == 2 || grid[p.i][p.j] == -1)
                continue;

            // if empty lot
            if (grid[p.i][p.j] == 0) {
                // mark seen
                grid[p.i][p.j] = -1;
                // add dist and reach
                distance[p.i][p.j] += p.dist;
                reach[p.i][p.j]++;

                // if 0 or 1  --> 1 is only first time
                q.offer(new Point(p.i + 1, p.j, p.dist + 1));
                q.offer(new Point(p.i - 1, p.j, p.dist + 1));
                q.offer(new Point(p.i, p.j + 1, p.dist + 1));
                q.offer(new Point(p.i, p.j - 1, p.dist + 1));
            }
        }

        // clean up to original grid
        for (int x = 0; x < grid.length; x++)
            for (int y = 0; y < grid[0].length; y++)
                if (grid[x][y] == -1)
                    grid[x][y] = 0;
    }

    public static void main(String[] args) {
//        int[][] grid = new int[][]{
//                {1 , 0 , 0 , 1 , 2},
//                {0 , 2 , 0 , 0 , 0},
//                {0 , 0 , 2 , 2 , 0},
//                {0 , 0 , 0 , 0 , 0},
//                {1 , 2 , 1 , 2 , 1},
//        };
        int[][] grid = new int[][]{{1,0,1}};
        ShortestDistanceFromAllBuildings s  = new ShortestDistanceFromAllBuildings();
        System.out.println(s.shortestDistance(grid));
    }

    class Point{
        int i;
        int j;
        int dist;

        public Point(int i, int j, int dist) {
            this.i = i;
            this.j = j;
            this.dist = dist;
        }
    }
}