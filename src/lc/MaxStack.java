package lc;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;
import java.util.TreeMap;

/*
Two Ways:
1- single STACK that keeps diff with max. similar to MinStack
    keep difference between max and self in stack
    time and space complexity:  O(1) for all; except popMax() that is O(n) time and space

2- TreeMap & DoubleLinkedList
*/

// 1- Single Stack which keeps diff with max
public class MaxStack {
    Stack<Integer> stk;
    int max;

    public MaxStack() {
        this.stk = new Stack<>();
    }

    public void push(int x) {
        if (stk.isEmpty()){
            stk.push(0);
            max = x;
        } else{
            stk.push(x - max);
            max = (x > max) ? x : max;
        }
    }

    public int pop() {
        int pop = stk.pop();
        if (pop <= 0 )
            return max + pop; // e.g. 8 + -3 = 5
        else{
            int ret = max;
            max -= pop;
            return ret;
        }
    }

    public int top() {
        int peek = stk.peek();
        if (peek <= 0 )
            return max + peek; // e.g. 8 + -3 = 5
        else
            return max;
    }

    public int peekMax() {
        return max;
    }

    public int popMax() {
        Stack<Integer> temp = new Stack<>();
        int oldMax = max;

        // until one bef
        while(top() != oldMax)
            temp.push(pop());

        pop();

        while(!temp.isEmpty())
            push(temp.pop());

        return oldMax;
    }

    public static void main(String[] args) {
        MaxStack maxStack = new MaxStack();
        int[] inputs = {2,1,3,4,5,6,7,8,9,2,4};
        for (int n: inputs) {
            System.out.println("input is:"+n);
            maxStack.push(n);
            System.out.println(maxStack.peekMax());
        }

        System.out.println("popmax:"+maxStack.popMax());

        while (!maxStack.stk.isEmpty()) {
            System.out.print("current max:"+maxStack.peekMax());
            System.out.println(" cur pop:"+maxStack.pop());
        }
    }


    /*

    https://leetcode.com/articles/max-stack/

    second approach:   O(logn) time,  O(n) space

    use a TreeMap mapping values to a list of nodes to answer this question.
    TreeMap can find the largest value, insert values, and delete values, all in O(logN) time.

lc.MaxStack.push(x): add a node to our dll, and add or update our entry map.get(x).add(node).

lc.MaxStack.pop(): find the value val = dll.pop(), and remove the node from our map, deleting the entry if it was the last one.

lc.MaxStack.popMax(): use the map to find the relevant node to unlink, and return it's value.
     */
    class MaxStackTreeMap {
        TreeMap<Integer, List<Node>> map;
        DoubleLinkedList dll;

        public MaxStackTreeMap() {
            map = new TreeMap();
            dll = new DoubleLinkedList();
        }

        public void push(int x) {
            Node node = dll.add(x);
            if(!map.containsKey(x))
                map.put(x, new ArrayList<Node>());
            map.get(x).add(node);
        }

        public int pop() {
            int val = dll.pop();
            List<Node> L = map.get(val);
            L.remove(L.size() - 1);
            if (L.isEmpty()) map.remove(val);
            return val;
        }

        public int top() {
            return dll.peek();
        }

        public int peekMax() {
            return map.lastKey();
        }

        public int popMax() {
            int max = peekMax();
            List<Node> L = map.get(max);
            Node node = L.remove(L.size() - 1);
            dll.unlink(node);
            if (L.isEmpty()) map.remove(max);
            return max;
        }
    }

    class DoubleLinkedList {
        Node head, tail;

        public DoubleLinkedList() {
            head = new Node(0);
            tail = new Node(0);
            head.next = tail;
            tail.prev = head;
        }

        public Node add(int val) {
            Node x = new Node(val);
            x.next = tail;
            x.prev = tail.prev;
            tail.prev = tail.prev.next = x;
            return x;
        }

        public int pop() {
            return unlink(tail.prev).val;
        }

        public int peek() {
            return tail.prev.val;
        }

        public Node unlink(Node node) {
            node.prev.next = node.next;
            node.next.prev = node.prev;
            return node;
        }
    }

    class Node {
        int val;
        Node prev, next;
        public Node(int v) {val = v;}
    }
}