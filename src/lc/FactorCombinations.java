package lc;

import java.util.ArrayList;
import java.util.List;

// lc 254
public class FactorCombinations {

    public static List<List<Integer>> factorCombs(int n){
        List<List<Integer>> ans = new ArrayList();
        List<Integer> tmp = new ArrayList();
        f(ans,tmp,n,2);
        return ans;
    }
    private static void f(List<List<Integer>> ans, List<Integer> tmp, int n, int st){
        // n==1 means one combination found. if size is 1 dump it cuz it's final ans [n]
        if (n == 1 && tmp.size()>1)
            ans.add(new ArrayList(tmp));

        for (int i = st; i<=n; i++){
            if (n%i == 0){
                tmp.add(i);
                f(ans,tmp,n/i,i);
                tmp.remove(tmp.size()-1);
            }
        }
    }

    public static void main(String[] args) {
        System.out.println(factorCombs(90));
    }
}
