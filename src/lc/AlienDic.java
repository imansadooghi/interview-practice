package lc;

import java.util.*;

// lc 269

/*
 *  idea: create a graph ->  char -> Set of postChars           key:charIndex in english-alphabet   val: Set of chars after it in Alien
 *        also, inDegree of each char, number of its parents    -> int[] array for inDegrees
 *
 *       Albatross like solution.
 *
 *       start from indegree 0s -> push to queue.  poll from queue. update children inDegrees, push...
 *
 *
 *
 *
 *       NOTE: To me it makes sense, if a char is in a string, not playing a role in comparison, we should not add it to result! cuz we dont actually know their rank!
 *       other solutions add them as rank 0
 *
 *       for graph, instead of HashMap, we can use array[26] of HashSets -> HashSet<Character>[]
 *
 *      degSize is to check sanity, makes sure all the chars in alphabet were used and there is no loop.
 *      if assumed the input is valid, no need for degSize
 *
 *      Edge case: if a pattern shows up twice, DONT increase InDegree twice
 *      Edge: bad input of  "abc","ab"
 * */
public class AlienDic {

    public static String alienOrder(String[] words) {
        Map<Character,Set<Character>> graph = new HashMap<>();
        Map<Character, Integer> inDegree = new HashMap();

        for (String w : words) {
            for (char c : w.toCharArray()) {
                inDegree.put(c,0);
            }
        }

        // extract patterns and build the graph
        for (int i = 0; i < words.length - 1; i++) {
            // edge case, Check that b is not a prefix of a. => wrong input. cuz it should come after
            if (words[i].length() > words[i+1].length() && words[i].startsWith(words[i+1])) {
                return "";
            }

            char[] edge = extractPattern(words[i], words[i + 1]);
            if (edge.length > 0) {
                Set<Character> post = graph.getOrDefault(edge[0],new HashSet());
                if (post.add(edge[1])){
                    graph.put(edge[0],post);
                    inDegree.put(edge[1], inDegree.get(edge[1])+1);
                }
            }
        }


        Queue<Character> q = new LinkedList<>();

        for (char c : inDegree.keySet())
            if (inDegree.get(c) == 0)
                q.add(c);

        StringBuilder sb = new StringBuilder();
        while (!q.isEmpty()) {
            char p = q.poll(); // parent
            sb.append(p);
            if (graph.containsKey(p)) {
                for (char c : graph.get(p)) { //child
                    inDegree.put(c,inDegree.get(c)-1);
                    if (inDegree.get(c) == 0) {
                        q.add(c);
                    }
                }
            }
        }
        return (sb.length() == inDegree.size()) ? sb.toString() : "";
    }


    // if a is substring of b -> no pattern can be extracted from these two.
    //
    public static char[] extractPattern(String a, String b) {
        int len = Math.min(a.length(), b.length());
        for (int i = 0; i < len; i++)
            if (a.charAt(i) != b.charAt(i))
                return new char[]{a.charAt(i), b.charAt(i)};

        return new char[]{};
    }


    public static String alienOrderARRAYMemHasBUG(String[] words) {
        HashSet<Character>[] graph = new HashSet[26];
        int[] inDegree = new int[26];

        int degSize = 0;
        Arrays.fill(inDegree,-1);
        for (String w : words) {
            for (char c : w.toCharArray()) {
                if (inDegree[c - 'a'] < 0) {
                    inDegree[c - 'a'] = 0;
                    degSize++;
                }
            }
        }

        // extract patterns and build the graph
        for (int i = 0; i < words.length - 1; i++) {
            char[] edge = extractPattern(words[i], words[i + 1]);
            if (edge.length > 0) {
                if (graph[edge[0] - 'a'] == null)
                    graph[edge[0] - 'a'] = new HashSet<>();
                graph[edge[0] - 'a'].add(edge[1]);
                inDegree[edge[1] - 'a']++;
            }
        }


        Queue<Character> q = new LinkedList<>();

        for (int i = 0; i < 26; i++)
            if (inDegree[i] == 0)
                q.add((char) ('a' + i));

        StringBuilder sb = new StringBuilder();
        while (!q.isEmpty()) {
            char p = q.poll(); // parent
            sb.append(p);
            if (graph[p - 'a'] == null)
                continue;
            for (char c : graph[p - 'a']) { //child
                if (--inDegree[c - 'a'] == 0)
                    q.add(c);
            }
        }
        return (sb.length() == degSize) ? sb.toString() : "";
    }

    public static void main(String[] a) {
        String[] words = {
                "wrt",
                "wrf",
                "et",
                "ett",
                "rftbt"};

//        System.out.println(alienOrderHM(words));
//        System.out.println(alienOrderReference(words));
//        System.out.println(alienOrderHM(new String[]{"wrt", "wrf", "er", "ett", "rftt", "bb"}));
//        System.out.println(alienOrderReference(new String[]{"wrt", "wrf", "er", "ett", "rftt", "bb"}));
        System.out.println(alienOrder(new String[]{"za","zb","ca","cb"}));


    }


    public static class GroupShiftedStrings {

    /* LC249
    *  https://www.programcreek.com/2014/05/leetcode-group-shifted-strings-java/
    * */

        /* the only point is the rotation.  za = ab,  ba = az
        * solution: if value is less the previous char:  calculate
        */
        private static String getCode(String word){
            if (word.length() == 0 || word.length() == 1)
                return " ";
            char[] arr = word.toCharArray();
            StringBuffer sb = new StringBuffer();
            for (int i=1;i<arr.length;i++){
                int diff = arr[i] - arr[i-1];
                if (diff < 0)
                    diff += 26;  // + 1 is important.  sum by a full round of alphabets
                sb.append(diff);
            }
            return sb.toString();
        }
        /*
    idea:  encode each word use this formula -> diff of each letter and the letter before it. abc -> 111
    */
        public static List<List<String>> groupShiftedStrings(List<String> words) {
            Map<String,List<String>> map = new HashMap();
            for (String w : words){
                String code = getCode(w);
                map.putIfAbsent(code, new ArrayList());
                map.get(code).add(w);
            }

            List<List<String>> ans = new ArrayList();
            for (String code : map.keySet())
                ans.add(map.get(code));

            return ans;
        }

        public static void main(String[] args) {
            List<String> input = Arrays.asList("abc", "bcd", "acef", "xyz", "azzabc", "baabcd", "a", "z", " ", "");
            List<List<String>> ans = groupShiftedStrings(input);
            System.out.println(ans);
        }

    }
}

