package lc;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;
import java.util.Queue;
/*
* lc 253
Given an array of meeting time intervals consisting of start and end times [[s1,e1],[s2,e2],...]
 find the minimum number of conference rooms required.

Idea:
    - SORT based on beginnings
    - PQ keeps rooms that are overlapping at each time.
    - PQ is sorted based on ENDs, meaning:  the one with EARLIER End is on Top.
    each time:
     - compare current with top of PQ. if cur's start is after their end, take them out and put cur inside.

    at end size of PQ is answer.
 * */
class MeetingRoomsII {
    public static int minMeetingRooms(int[][] ivs) {
        if (ivs.length == 0)
            return 0;

        Arrays.sort(ivs,(a,b) -> (a[0] - b[0]));
        Queue<int[]> pq = new PriorityQueue<>((a,b) -> (a[1]-b[1]));
        pq.add(ivs[0]);

        for (int i=1; i<ivs.length;i++) {
           int[] last = pq.peek();
           if (last[1] > ivs[i][0])
               pq.add(ivs[i]);
           else {
               pq.poll();
               pq.add(ivs[i]);
           }
        }
        return pq.size();
    }









































        public static int minMeetingRooms(Interval[] intervals) {
        if (intervals.length == 0)
            return 0;

        PriorityQueue<Integer> pq = new PriorityQueue();

        Arrays.sort(intervals, (Interval o1, Interval o2) -> o1.start - o2.start);

        pq.add(intervals[0].end);

        for (int i=1; i<intervals.length;i++) {
            int s = intervals[i].start;
            if (s >= pq.peek())
                pq.remove();
            pq.add(intervals[i].end);
        }

        return pq.size();
    }

    public static void main(String[] args) {
        Interval i1 = new Interval(0,10);
        Interval i2 = new Interval(5,9);
        Interval i3 = new Interval(10,20);
        Interval i4 = new Interval(10,15);
        Interval i5 = new Interval(10,12);
        Interval i6 = new Interval(10,17);

        Interval[] is = {i2,i1,i4,i3,i5,i6};
        System.out.println(minMeetingRooms(is));

        int[][] ivs = new int[][]{{0,10},{5,9},{10,20},{10,15},{10,12},{10,17}};
        System.out.println(minMeetingRooms(ivs));

       ivs = new int[][]{{0,10},{5,9},{0,20},{14,15},{15,22},{21,27}};
        System.out.println(minMeetingRooms(ivs));



    }
    public static class Interval {
        int start;
        int end;
        Interval() { start = 0; end = 0; }
        Interval(int s, int e) { start = s; end = e; }
        public String toString(){
            return String.valueOf(start)+"->"+String.valueOf(end);
        }

    }
}