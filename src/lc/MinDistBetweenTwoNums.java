package lc;

import java.util.*;

public class MinDistBetweenTwoNums {
/*
    https://www.geeksforgeeks.org/find-the-minimum-distance-between-two-numbers/

    same as leetcode 243: shortest word distance.

*/

    // my first solution. came to me by looking at example   --> 3,x,x,x,6,x,6,6,x,x,x,3
    public int minDistBad(int[] n, int x, int y){
        int dist = n.length;
        int ix = -1, iy = -1;

        for (int i=0; i<n.length; i++){
            if (n[i] == x){
                if (ix == -1 || ix < i)
                    ix = i;
                if (iy != -1)
                    dist = Math.min(dist, ix - iy);
            } else if (n[i] == y){
                if ( iy == -1 || iy < i)
                    iy = i;
                if (ix != -1)
                    dist = Math.min(dist, iy - ix);
            }
        }
        return dist;
    }

    // in above, no need to check if ix < i || ix = -1 -> it always is less.
    public int minDist(int[] n, int x, int y) {
        int ix = -1, iy=-1;
        int dist = n.length;

        for (int i = 0; i < n.length; i++) {
            if (n[i] == x){
                ix = i;
                if (iy != -1)
                    dist = Math.min(dist, ix - iy);
            }

            if (n[i] == y){
                iy = i;
                if (ix != -1)
                    dist = Math.min(dist, iy - ix);
            }
        }

        return dist;
    }

        public int minDistClean(int[] n, int x, int y) {
        int dist = n.length;
        // first of x and y
        int first = -1;

        for (int i=0; i<n.length; i++) {
            if (n[i] == x || n[i] == y){
                if (first != -1 && n[first] != n[i])
                    dist = Math.min(dist, i - first);
                first = i;
            }
        }


        return dist;
    }

    public static List<String> removeInvalidParentheses(String st) {
        Queue<String> q = new LinkedList();
        HashSet<String> seen = new HashSet();
        List<String> ans = new ArrayList();

        q.add(st);
        seen.add(st);

        while (!q.isEmpty() && ans.size() == 0){ // if last level found answers, stop here
            int size = q.size();
            for (int i=0; i<size;i++){
                String s = q.poll();

                if (isValid(s))
                    ans.add(s);
                else if (ans.size() == 0){ // if a previous string in this level already found answer, dont add
                    for (int j = 0; j<s.length(); j++){
                        String sub = s.substring(0,j) + s.substring(j+1);
                        if (seen.add(sub))
                            q.add(sub);
                    }
                }
            }
        }

        return ans;
    }

    private static boolean isValid(String s){

        int i = 0;
        for (char c : s.toCharArray()){
            if (i < 0)
                return false;
            if (c == '(')
                i++;
            else if (c == ')')
                i--;
        }
        System.out.println(s+" isval:"+ (i==0));

        return i == 0;
    }

    public static void main(String[] args){
        MinDistBetweenTwoNums o = new MinDistBetweenTwoNums();

        int[] n = {3, 5, 4, 2, 6, 5, 6, 6, 5, 4, 8, 3};
        int x = 3, y = 6;

//        int[] n = {2,4,4,2,2,2,8,3,5,3,3,3,4,5,6,3, 5, 4, 4, 3};
//        int x = 3, y = 2;
//        System.out.println( o.minDist(n,x,y));
//        System.out.println( o.minDistClean(n,x,y));
        System.out.println(removeInvalidParentheses("()())()"));

    }
}

