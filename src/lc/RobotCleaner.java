package lc;

import java.util.HashSet;
import java.util.Set;
/*
Given a robot cleaner in a room modeled as a grid.

Each cell in the grid can be empty or blocked.

The robot cleaner with 4 given APIs can move forward, turn left or turn right. Each turn it made is 90 degrees.

When it tries to move into a blocked cell, its bumper sensor detects the obstacle and it stays on the current cell.

Design an algorithm to clean the entire room using only the 4 given APIs shown below.

interface Robot {
  // returns true if next cell is open and robot moves into the cell.
  // returns false if next cell is obstacle and robot stays on the current cell.
  boolean move();

  // Robot will stay on the same cell after calling turnLeft/turnRight.
  // Each turn will be 90 degrees.
  void turnLeft();
  void turnRight();

  // Clean the current cell.
  void clean();
}

*  lc 489
*  idea: since we are blindfolded, we need to have our own sense of location and direction.
*
*
*  4 Steps: Clean & Mark   then    Move(in 4 dirs) & RecurseF()  &  ComeBack
*
*        initially, you have a location and a direction that you dont know about. we assume we are at 0,0  and going down (1,0)
*        each time:
*        1 - clean, and mark the current location visited
*           try the following 4 times: for all four turns:
*           2 - try to move to the next location with the current direction.
*               if successful: 2.1 - call the same process from 1.
*                              2.2 - move back to current location.
*        3 - turn (and update your perception of direction)  (you could not move or nextPotential location is already visited. so need to turn)
*
*
*       things forgot on second attempt:
*       1- the for loop for 4 directions **VERY IMPORTANT** ->
*       for each location, have to try to go forward in all 4 directions before exiting function!
*       WHY? if forward is blocked or visited, turn and go all the way.
*       also dont worry about backtracking after a turn. a turn is in the middle of an incomplete attempt from the ancestor func call.
*
*       2- directions array: just keeps for separate rows.
*
*       3- hashset: simply save string "x:y"
* */
public class RobotCleaner {

    Set<String> visited;
    private final int[][] dirs = {{1,0},{0,-1},{-1,0},{0,1}};
    int currentDir;
    // actual solution
    public void cleanRoom(Robot robot) {
        visited = new HashSet<>();
        currentDir = 0;
        f(robot,visited,0,0);
    }



    public void f(Robot robot, Set<String> visited, int x, int y){
        String loc = x +"-"+y;
        visited.add(loc);
        robot.clean();

        for (int i=0;i<4;i++){
            int nextX = x + dirs[currentDir][0];
            int nextY = y + dirs[currentDir][1];
            String nextLoc = nextX+"-"+nextY;
            // if moveable and not visited, move and clean
            if (!visited.contains(nextLoc) && robot.move()){
                f(robot,visited,nextX,nextY);
                //move back to here with current direction
                robot.turnLeft();
                robot.turnLeft();
                robot.move();
                robot.turnLeft();
                robot.turnLeft();
            }
            //turn and update dir
            currentDir = (currentDir + 1) % 4;
            robot.turnRight();
        }
    }

    interface Robot {
        // returns true if next cell is open and robot moves into the cell.
        // returns false if next cell is obstacle and robot stays on the current cell.
        boolean move();

        // Robot will stay on the same cell after calling turnLeft/turnRight.
        // Each turn will be 90 degrees.
        void turnLeft();
        void turnRight();

        // Clean the current cell.
        void clean();

        void checkCleanliness();
    }

    static class ActualCleaner implements Robot {

        int[][] room;
        boolean[][] roomClean;

        int i, j;
        private final int[][] dirs = {{1,0},{0,1},{-1,0},{0,-1}};
        int currentDir;
        public ActualCleaner(int[][] room, int i, int j) throws Exception {
            this.room = room;
            this.i = i;
            this.j = j;
            currentDir = 0;
            roomClean = new boolean[room.length][room[0].length];

            if (i < 0 || i >= room.length || j <0 || j >= room[0].length
                    || room[i][j] == 0)
                throw new Exception("invalid initial location i&j");
        }

        @Override
        public boolean move() {
            int iNew = i + dirs[currentDir][0];
            int jNew = j + dirs[currentDir][1];

            if (iNew < 0 || iNew >= room.length || jNew <0 || jNew >= room[0].length
                    || room[iNew][jNew] == 0)
                return false;
            else{
                i = iNew;
                j = jNew;
                return true;
            }
        }

        @Override
        public void turnLeft() {
            currentDir = (currentDir+1) % 4;
        }

        @Override
        public void turnRight() {
            currentDir = (currentDir+3) % 4;
        }

        @Override
        public void clean() {
            if (i < 0 || i >= room.length || j <0 || j >= room[0].length
                    || room[i][j] == 0)
                System.out.println("Invalid cell cleaning attempt for i:"+i+" j:"+j);
            else
                roomClean[i][j] = true;
        }

        public void checkCleanliness(){
            for (int k = 0; k < roomClean.length ; k++) {
                for (int l = 0; l < roomClean[0].length; l++)
                    System.out.print( (room[k][l] == 0 ? "X" : (roomClean[k][l] ? "c":"d")) + ","); // clean or dirty or X
                System.out.println();
            }
        }
    }

    public static void main(String[] args) throws Exception {
        int[][] room = {
                {1,1,0,1,1,1,0,1},
                {1,1,0,1,1,0,1,1},
                {1,1,1,1,1,0,1,0},
                {0,0,1,1,1,0,0,1},
                {0,1,1,1,1,0,0,1},
                {0,1,1,1,1,1,1,1},
                {0,1,1,1,1,1,0,1},
                {0,1,1,1,1,1,0,1}};

        Robot robot = new ActualCleaner(room,2,2);

        RobotCleaner rc = new RobotCleaner();
        rc.cleanRoom(robot);

        robot.checkCleanliness();
    }
}
