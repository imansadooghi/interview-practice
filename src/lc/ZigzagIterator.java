package lc;

/*
this works for the follow up of n lists!

idea: use a queue of ITERATORS -> add all iters to it

    on each next -> get front of queue -> take next() from it, if hasNext() push it back to end of the Queue

    Only add NON Empty Iterators to Queue

*/


import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class ZigzagIterator {
    Queue<Iterator> q;

    public ZigzagIterator(List<Integer> v1, List<Integer> v2) {
        q = new LinkedList<Iterator>();
        if (!v1.isEmpty()) q.add(v1.iterator());
        if (!v2.isEmpty()) q.add(v2.iterator());
    }

    public int next() {
        if (!hasNext())
            throw new RuntimeException();
        Iterator it = q.poll();
        int nextInt = (Integer) it.next();
        if (it.hasNext())
            q.add(it);

        return nextInt;
    }

    public boolean hasNext() {
        return !q.isEmpty();
    }
}



/**
 * Your ZigzagIterator object will be instantiated and called as such:
 * ZigzagIterator i = new ZigzagIterator(v1, v2);
 * while (i.hasNext()) v[f()] = i.next();
 */