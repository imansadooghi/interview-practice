package lc;

import java.util.HashMap;
import java.util.HashSet;

// https://leetcode.com/submissions/detail/95614264
public class TwoSum {
    private HashMap<Integer,Integer> nums;

    // fast find
    private HashSet<Integer> ns;
    private HashSet<Integer> sums;

    /** Initialize your data structure here. */
    public TwoSum() {
        nums = new HashMap<>();
        ns = new HashSet<>();
        sums = new HashSet<>();
    }

    /** Add the number to an internal data structure.. */
    public void add(int num) {
        if (nums.containsKey(num))
            nums.put(num,2);
        else
            nums.put(num,1);

        // second way
        for (int i : ns)
            sums.add(i + num);
        ns.add(num);

    }

    /** Find if there exists any pair of numbers which sum is equal to the value. */
    public boolean find(int v) {
        for (int k: nums.keySet())
            if (nums.containsKey(v-k) && ( (k != v - k) || nums.get(k) == 2))
                return true;
        return false;
    }

    public boolean find2(int num) {
        return sums.contains(num);
    }

        public static void main(String[] args){
        TwoSum t = new TwoSum();

        t.add(1);
        t.add(4);
        t.add(3);
        t.add(5);
        t.add(1);


        System.out.println(t.find(10));
        System.out.println(t.find2(10));
        System.out.println(t.find(1));
        System.out.println(t.find2(1));
        System.out.println(t.find(2));
        System.out.println(t.find2(2));
        System.out.println(t.find(5));
        System.out.println(t.find2(5));
        System.out.println(t.find(7));
        System.out.println(t.find2(7));
        System.out.println(t.find(8));
        System.out.println(t.find2(8));
        System.out.println(t.find(6));
        System.out.println(t.find2(6));


    }
}