package lc;
/*
Idea:  create a lazy graph of direct syns as neighbors.
       then walk backtracking style-> check if word is in graph( has syns)
                 - if has -> dfs and get all deep syns (direct and indirect)
                    -> continue with all syns
                - else -> just go forward.  NOTE: still need to remove it after backtrack


Points:
    - asks for sorted: use treeSet when doing dfs
    - on traverse, use List -> at end make sentence

*/

import java.util.*;

class SynonymSentences {
    public List<String> generateSentences(List<List<String>> syns, String text) {
        Map<String, List<String>> g = new HashMap();

        //build shallow graph. only direct syns
        for (List<String> pair : syns)
            connect(g, pair.get(0), pair.get(1));

        List<String> ans = new ArrayList();
        // traverse on words of split
        traverse(text.split(" "), 0, g, new ArrayList(), ans);

        return ans;
    }

    // dfs method to gather all neighbors/synonyms
    void dfs(String w, Map<String, List<String>> g, Set<String> res){
        for ( String nei: g.get(w)){
            if (!res.contains(nei)){
                res.add(nei);
                dfs(nei,g,res);
            }
        }
    }

    void traverse(String[] split, int i, Map<String, List<String>> g, List<String> part, List<String> ans){
        if (i == split.length){
            ans.add(String.join(" ",part));
            return;
        }

        if (g.containsKey(split[i])){ // word has syns
            // first get all deep/indirect syns
            Set<String> syns = new TreeSet();
            dfs(split[i], g, syns);
            // now loop through and make 1 sentence permutation with each
            for (String syn : syns){
                part.add(syn);
                traverse(split,i+1,g,part,ans);
                part.remove(part.size()-1);
            }
        } else { // no syns exist
            part.add(split[i]);
            traverse(split,i+1,g,part,ans);
            part.remove(part.size()-1);
        }
    }

    void connect(Map<String, List<String>> graph, String v1, String v2) {
        graph.putIfAbsent(v1, new LinkedList<>());
        graph.get(v1).add(v2);

        graph.putIfAbsent(v2, new LinkedList<>());
        graph.get(v2).add(v1);
    }

    /* NOT MINE, for reference
    puts whole sentence in queue,
    - each time walks on sentence -> if a syn word found -> swaps the word with all syns and CHECK if not exist
    */

    public List<String> generateSentencesBFS(List<List<String>> synonyms, String text) {
        Map<String, List<String>> graph = new HashMap<>();
        for (List<String> pair : synonyms) {
            String w1 = pair.get(0), w2 = pair.get(1);
            connect(graph, w1, w2);
        }
        // BFS
        Set<String> ans = new TreeSet<>();
        Queue<String> q = new LinkedList<>();
        q.add(text);
        while (!q.isEmpty()) {
            String out = q.remove();
            ans.add(out); // Add to result
            String[] words = out.split("\\s");
            for (int i = 0; i < words.length; i++) {
                if (graph.get(words[i]) == null) continue;
                for (String synonym : graph.get(words[i])) { // Replace words[i] with its synonym
                    words[i] = synonym;
                    String newText = String.join(" ", words);
                    if (!ans.contains(newText)) q.add(newText);
                }
            }
        }
        return new ArrayList<>(ans);
    }

}