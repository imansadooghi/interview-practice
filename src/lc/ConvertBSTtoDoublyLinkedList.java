package lc;

import java.util.Stack;

public class ConvertBSTtoDoublyLinkedList {
/*

 lc 426

* Convert a BST to a sorted circular doubly-linked list in-place. Think of the left and right pointers as synonymous to the previous and next pointers in a doubly-linked list.

Let's take the following BST as an example, it may help you understand the problem better:





We want to transform this BST into a circular doubly linked list. Each node in a doubly linked list has a predecessor and successor. For a circular doubly linked list, the predecessor of the first element is the last element, and the successor of the last element is the first element.

The figure below shows the circular doubly linked list for the BST above. The "head" symbol means the node it points to is the smallest element of the linked list.





Specifically, we want to do the transformation in place. After the transformation, the left pointer of the tree node should point to its predecessor, and the right pointer should point to its successor. We should return the pointer to the first element of the linked list.

The figure below shows the transformed BST. The solid line indicates the successor relationship, while the dashed line means the predecessor relationship.


* */

    static Node treeToDoublyListStack(Node root) {
        if (root == null) return null;

        Node head = null, prev = null;
        Stack<Node> st = new Stack<>();

        while (root != null || !st.isEmpty()) {
            while (root != null) {
                st.push(root);
                root = root.left;
            }

            root = st.pop();
            // left-most -> point head to first node
            if (head == null) head = root;
            //
            if (prev != null) {
                prev.right = root;
                root.left = prev;
            }
            prev = root;
            root = root.right;
        }

        // connect head to tail. make it circular
        head.left = prev;
        prev.right = head;

        return head;
    }


    private static Node prev = null;
    private static Node head = null;
//inorder:  idea: if we traverse indorder, and keep a global prev.
//              each time, connect the prev to your left.
                         // and make yourself prev.
    public static Node treeToDoublyList(Node root) {
        if (root == null) return null;

        inorder(root);
        head.left = prev;
        prev.right = head;
        return head;
    }

    private static void inorder(Node node) {
        if (node == null) {
            return;
        }

        inorder(node.left);
        if (prev == null) {
            head = node;
        }else {
            prev.right = node;
            node.left = prev;
        }

        prev = node;

        inorder(node.right);
    }

    public static void main(String[] args) {
        Node one = new Node(1);
        Node two = new Node(2);
        Node three= new Node(3);
        Node four= new Node(4);
        Node five= new Node(5);
        Node six= new Node(6);
        Node seven= new Node(7);
        Node eight= new Node(8);

        four.right = five;
        four.left = two;
        two.right = three;
        two.left = one;

        six.left = four;
        six.right = eight;
        eight.left = seven;

       // Node first = treeToDoublyListStack(six);
        Node first = treeToDoublyList(six);
        while(first.right != null) {
            System.out.println(first);
            first = first.right;
        }
    }

    static class Node {
        public int val;
        public Node left;
        public Node right;

        public Node(int v) {
            val = v;
        }

        public Node(int _val,Node _left,Node _right) {
            val = _val;
            left = _left;
            right = _right;
        }

        @Override
        public String toString() {
            return String.valueOf(val);
        }
    };
}

/*
BAD FIRST TRY

    public static Node treeToDoublyList(Node root) {
        conv(root.right,0);
        conv(root.left,1);

        while(root.left != null)
            root = root.left;
        return root;
    }

    // dir: 0:from left to right   1:from right to left
   static Node  conv(Node node, int dir){
        if (node == null || (node.left == null && node.right == null))
            return node;

        Node newl = conv(node.left, 1);
        Node newr = conv(node.right, 0);

        if (newl != null){
            node.left = newl;
            newl.right = node;
        }

        if (newr != null){
            node.right = newr;
            newr.left = node;
        }

        if (dir == 0)
            return (newl == null) ?  node : newl;
        else
            return (newr == null) ?  node : newr;
    }

*/


