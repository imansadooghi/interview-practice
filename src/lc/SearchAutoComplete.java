package lc;

import java.util.*;

/* lc 642        note: using LFU instead of List is possible if k is significantly bigger than just 3 -> o(nlogn) to o(n)


Solution idea: autocomplete => Trie. but for each TrieNode, keep top-k within the TrieNode. if small, just a simple arrayList is ok.

init: fill HashMap of counts & create the trie with inserts -> all inserts are first time inserts of these Strings.

insert: keep adding to TrieNodes. extra action is to update each node's top-k list if possible.

on update: 2 scenarios:
 - new node being inserted: -> on init or after '#'
 - existing node being inserted: -> potentially could be in top-k list -> so need to check the list!

Note about update: if top-k was bigger -> we could improve by using LFU instead -> O(1) lookups and updates

for input():
    if '#':
        - Insert the current sentence to Trie with its frequency:
                                - if brand new -> hashMap shouldnt have it => freq = 1;  -> this will potentially build new leaves and branches within Trie
                                - if existing -> hashMap has it => old freq + 1;
        - reset cur and sb
        - return an empty list

    if other chars:
        - regardless of existence of input,  add char to sb.
        - Tricky part: if non-existing sentence. how to handle? -> make sure you make the cur = null -> and then handle null cases for cur.
        - if cur is null =>  return an empty list.
        - else: cur = cur.child  -> the child could be null. =>
                                                                -null? return empty list
                                                                -not-null? return the top3 list of that child


Alternatively, we could use a rootCount field in TrieNodes instead of a global HashMap. => slightly different impl. available here:
https://happygirlzt.com/code/642.html

*/
public class SearchAutoComplete {
    static TrieNode root;
    TrieNode cur;
    StringBuilder sb;
    HashMap<String,Integer> counts;

    public SearchAutoComplete(String[] sentences, int[] times) {
        counts = new HashMap<>();
        root = new TrieNode();
        cur = root;
        sb = new StringBuilder();

        for (int i = 0; i < sentences.length; i++) {
            counts.putIfAbsent(sentences[i],times[i]);
            insert(sentences[i]);
        }
    }

    //FIXME: has a bug -> non-existing followed by wrong existing  -> 'a' returns [] then 'i' returns [i love you, ib, is]
    // FIXME: rewrite this nicer
    public List<String> input(char c) {
        if (c == '#'){
            String sentence = sb.toString();
            int count = counts.getOrDefault(sentence, 0);
            counts.put(sentence,count+1);
            insert(sentence);

            cur = root;
            sb = new StringBuilder();

            return new ArrayList<>();
        }
        // add char to new sentence being made
        sb.append(c);

        int index = (c == ' ') ? 26 : c- 'a';
        // if search doesnt exist -> return! this is a non-existing entry continuing
        if ( cur == null){
            return new ArrayList<>();
        } else         // move cur for next move, -> IT COULD MAKE IT NULL IF IT DOESNT EXIST!
            cur = cur.children[index];

        //VERY IMPORTANT for above mentioned bug
        return (cur == null) ? new ArrayList<>() : cur.hot3;
    }

    // Tricky part: if insert is into an existing sentence, => add 1 to it
    // important part is: make sure hashmap is updated before calling this method!
    private void insert(String sentence){
        TrieNode node = root;

        char[] chars = sentence.toCharArray();
        for (char c : chars){
            int index = (c == ' ') ? 26 : c - 'a';
            if (node.children[index] == null)
                node.children[index] = new TrieNode();

            node = node.children[index];
            node.updateHot3(sentence);
        }
        node.sentence = sentence;
    }


    class TrieNode {
        TrieNode[] children;
        String sentence; // this is also a leaf Indicator if not empty
        List<String> hot3;

        public TrieNode(){
            // 0-25 -> a-z   26:' ' space
            children = new TrieNode[27];
            hot3 = new ArrayList<>();
        }

        public void updateHot3(String sentence){
            // VERY IMPORTANT -> if already has it! dont add again. only sort again in case order within top 3 changes
            if (!hot3.contains(sentence))
                hot3.add(sentence);
            hot3.sort((a,b) -> counts.get(a) == counts.get(b) ? a.compareTo(b) : counts.get(b) - counts.get(a));

            if (hot3.size() > 3) hot3.remove(hot3.size()-1);
        }
    }


    public static void main(String[] args){
        String[] ss = {"i love you", "island","ironman", "i love leetcode", "i love me", "porto","isl"};
        int[] counts = {5,3,2,2,3,2,3};

        SearchAutoComplete searchAutoComplete = new SearchAutoComplete(ss,counts);

        Scanner sc = new Scanner(System.in);
        System.out.println("type:");
        while(sc.hasNextLine()) System.out.println(searchAutoComplete.input(sc.nextLine().charAt(0)));
    }


}
