package lc;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class MinDistBetweenTwoNumsII {
    /*
     *   this is extension of prev question. however we want to preprocess once and run MULTIPLE times
     *   it is same as leetcode 244:
     *   https://leetcode.com/articles/shortest-word-distance-ii
     * */
    // map of number and list of its appearance indexes
    Map<Integer, List<Integer>> locs;
    int[] nums;

    public MinDistBetweenTwoNumsII(int[] n) {
        nums = n;
        locs = new HashMap();
        for (int i = 0; i < n.length; i++) {
            List<Integer> indexes = locs.getOrDefault(n[i], new ArrayList<>());
            indexes.add(i);
            locs.put(n[i], indexes);
        }
    }

    public int minDist(int x, int y) {

        if (x == y)
            return 0;

        if (!locs.containsKey(x) || !locs.containsKey(y))
            return -1;

        List<Integer> lx = locs.get(x);
        List<Integer> ly = locs.get(y);

        int dist = nums.length;

        // ilx & ily are index of x and y within the list of indexes
        int ilx = 0, ily = 0;
        while (ilx < lx.size() && ily < ly.size()) {
            // ix & iy are are actual indexes on the original input array
            int ix = lx.get(ilx);
            int iy = ly.get(ily);

            dist = Math.min(dist, Math.abs(ix - iy));

            if (ix < iy)
                ilx++;
            else
                ily++;

        }

        return dist;
    }

    public static void main(String[] args) {
        int[] nums = {3, 5, 4, 2, 6, 5, 6, 6, 5, 4, 8, 3, 3, 3, 2, 6, 5, 6, 8,3,2,2,2,1,1,1,9,8};
        //int[] nums = {2,4,4,2,2,2,8,3,5,3,3,3,4,5,6,3, 5, 4, 4, 3};

        MinDistBetweenTwoNumsII o = new MinDistBetweenTwoNumsII(nums);

        System.out.println(o.minDist(3, 6));
        System.out.println(o.minDist(2, 6));
        System.out.println(o.minDist(4, 6));
        System.out.println(o.minDist(8, 4));
        System.out.println(o.minDist(2, 7));
        System.out.println(o.minDist(2, 3));
    }
}

