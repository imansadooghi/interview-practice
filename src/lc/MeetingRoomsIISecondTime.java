package lc;

import java.util.Arrays;
import java.util.Comparator;
import java.util.PriorityQueue;

public class MeetingRoomsIISecondTime {

    public static int minMeetingRooms(Interval[] in) {
        // Arrays.sort(intervals,(Interval a, Interval b) -> a.start - b.start);
        Arrays.sort(in);

        PriorityQueue<Integer> pq = new PriorityQueue();
        pq.add(in[0].end);

        for (int i=1; i<in.length;i++){
            int lastEnd = pq.peek();
            if (lastEnd <= in[i].start)
                pq.poll();
            pq.add(in[i].end);
        }

        return pq.size();
    }

    public static void main(String[] args) {
        Interval i = new Interval(0,10);
        Interval i2 = new Interval(5,9);
        Interval i3 = new Interval(10,20);
        Interval i4 = new Interval(10,15);
        Interval i5 = new Interval(10,12);
        Interval i6 = new Interval(10,17);

        Interval[] is = {i3,i,i2,i6,i5,i4};
        System.out.println(minMeetingRooms(is));
    }
    public static class Interval implements Comparable<Interval> {
        int start;
        int end;
        Interval() { start = 0; end = 0; }
        Interval(int s, int e) { start = s; end = e; }
        public String toString(){
            return String.valueOf(start)+"->"+String.valueOf(end);
        }


        @Override
        public int compareTo(Interval i) {
            return start - i.start;
        }
    }
}