package misc;/*

simulation program

- game
 - fair coin
 - keep tossing until three consecutive heads
 - example
   - HHH -> 3
   - HTTHHTHHH -> 9


*/


import java.io.*;
import java.util.*;

/*
 * To execute Java, please define "static void main" on a class
 * named Solution.
 *
 * If you need more classes, simply define them inline.
 */

class CoinToss {
    public static void main(String[] args) {
        ArrayList<String> strings = new ArrayList<String>();
        strings.add("Hello, World!");
        strings.add("Welcome to CoderPad.");


        System.out.println(toss(1000));

    }

    public static double toss(int n) {
        int heads = 0;
        int total = 0;
        int sum = 0;
        Random rand = new Random();

        for (int i=0; i<n; i++){
            while (heads < 3){
                if (rand.nextBoolean())
                    heads++;
                else
                    heads = 0;
                total++;
            }
            sum += total;
            heads = 0;
            total = 0;
        }

        return (double) sum/n;
    }
}
