package misc;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ExtractInts {

    public static Iterable<Integer> extractIntsRegex(String in){
        List<Integer> ans = new ArrayList<Integer>();

        Pattern p = Pattern.compile("[-+]?(\\d+)");
        Matcher m = p.matcher(in);

        while (m.find())
            ans.add(Integer.parseInt(m.group()));

        return ans;
    }

    /*
    *  idea: I came up with this just by looking at example: sdf234234-43-sdf--342--0
    *
    *   we either have non-digits or digits
    *
    *   when you see a digit: continue making the number! (or start making it. same thing)
    *   when you see a non digit: you need to do two things:
    *               - check if before it, there was a digit => add that to list!   e.g. 12312s or 12312-
    *                   note that even if it is -  -> this applies to it   123-
    *               - if this is a '-' -> check if there is a digit after it => turn on Negative.   e.g.  ~~-2 ~~-0
    *
    *   after while: you will potentially have the last number not added.
    *       - since number resets to 0 after each add, to avoid a case of:  123sd ->
    *           we only add if the last num was NOT 0  or  was zero && the actual char c == '0'
    *
    *
    *   Idea: all cases come down to:   - number
    *                                   - non-number:  - non-digit after a digit ->  3d or 3-
    *                                                  - negative sign before number -> -3
    *                                                  - both -> 3-3
    *                                                  - neither:   -- or sd  or -d
    *
    * */
    public static Iterable<Integer> extractInts(String in) {
        List<Integer> ans = new ArrayList<Integer>();
        int num = 0, len = in.length();
        boolean isNeg = false;
        int i=0;

        while(i<len){
            char c = in.charAt(i);
            if (Character.isDigit(c)){
                if (num > Integer.MAX_VALUE/10 ) // max int case
                    throw new NumberFormatException("For input String "+ num+ (c-'0'));
                num *= 10;
                num += c - '0';
            } else {
                if (i > 0 && Character.isDigit(in.charAt(i-1))){
                    ans.add(isNeg ? -num : num);
                    num = 0;
                    isNeg = false;
                }

                if (c == '-' && i< len-1 && Character.isDigit(in.charAt(i+1)) )
                    isNeg = true;
            }
            i++;
        }

        if (len > 0 && (in.charAt(len-1) == '0' || num != 0))
            ans.add(isNeg ? -num : num);


        return ans;
    }

        public static void main(String[] args) {
            String s = "as5+d92-020-12-p41+asd-2-21er9-1230-0-wwe2e2-92-343001-asd--2-0010--090123-11+-2dd00d00";
            System.out.println(extractIntsRegex(s));
            System.out.println(extractInts(s));
        }
}
