package misc;

public class SwitchGreaterNumberTwoNums {

    /*
    *
    *  Given 2 numbers for example 7223 and 2443, verify that the first number is smaller than the second.
    *  Then rearrange the number so that smaller number becomes the larger number.
    *
    *  Question claimed that This can be done using Heaps and converting the numbers to strings. I am not sure HOW
    *
    * */
    public static String switchGreaterWRONG(String s1, String s2){
        /*
        *  I thought I can solve O(nlogn).
        *  1- find first left digit in small that is smaller  e.g.  sm=4241 bg=4284  --> third: 4 < 8
        *  2- switch that number with the closest bigger number on the left of iNew in small. closest to digit in big.
        *       424176 425433   --> switch 4 with the closest bigger in 176 to 5 in big --> answer is 6 => 426174
        *  3- now sort the numbers to the right of switched digit 426'174 --> 426147
        *
        *  problem is in step 2 there could be no matching answer:  48415 & 48315 --> we need to go to right of 3. answer is 81345.
        * */
        return "";
    }

    /*
    *   find smaller. keep going to the next greater, until it is more.
    *   O(n*2)
    * */
    public static String switchGreater(String s1, String s2) {

        if (s1.length() != s2.length())
            return "not possible. not enough digits in the smaller number";

        // find the small and big. they could still be equal.
        String small = ( Integer.valueOf(s1) < Integer.valueOf(s2)) ? s1 : s2;
        String big = (small.equals(s1)) ? s2 : s1;

        while (Integer.valueOf(small) <= Integer.valueOf(big)) {
            String smTemp = nextGreater(small);
            if (smTemp.equals(small))
                return "not possible";
            small = smTemp;
        }
        return small;
    }
        public static String nextGreater(String num){
        if (num.length() < 2)
            return num;

        char[] n = num.toCharArray();

        // starting from right. look for first n[iNew] < n[iNew+1]     1232'32  2<3
        int i=n.length-2;
        while (i>=0 && n[i] >= n[i+1])
            i--;

        // if all descending  e.g. 4321
        if (i==-1)
            return num;

        // look for a bigger digit on the right of iNew
        int j = n.length-1;
        while (j>i && n[i] >= n[j])
            j--;

        // swap iNew & jNew
        char temp = n[j];
        n[j] = n[i];
        n[i] = temp;

        // sort the digits on right of iNew. since it is already descending -> reverse to make the ascending
        int l = i+1, r = n.length-1;
        while (l<r){
            temp = n[l];
            n[l++] = n[r];
            n[r--] = temp;
        }

        return new String(n);
    }

    public static void main(String[] args) {
        System.out.println(switchGreater("6226", "1892"));
        System.out.println(switchGreater("1", "2"));
        System.out.println(switchGreater("452821", "439121"));
        System.out.println(switchGreater("48415", "48315"));
        System.out.println(switchGreater("65432", "12345"));


    }
}
