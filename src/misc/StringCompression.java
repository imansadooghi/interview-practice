package misc;

public class StringCompression {
    /*
    *  Citadal: we have a decomp that does this:  if sees  [num][x][char] --> converts it to  that number of chars
    *
    *  write the compresser for it. conds: it has to do as small as possible when it can! aka: compressed cannot be longer than original(unless it has to I guess!?)!
    *
    *  3xa: aaa
    *  4xb: bbbb
    *
    *  if it sees other stuff. it just passes them as is.
    *
    *
    *  corner cases for compression:
    *
    *  1- input: 2xa --> general case will turn it into:  2xa (no compression possible). so:
    *                       2xa -comp-->  2xa --decomp--> aa    WRONG
    *                       we need to instead do this:  2xa -comp-->  1x2xa --decomp--> 2xa
    *  pattern: NON-compressable number + 'x' + char
     *
    * 2- input: 213aaaa --> general case -> 2134xa  -decomp-> aa...atimes2134...aa
    *
    * we need to compress it to 213aaaa --> -comp-> 1x21x11x34xa -decomp-> 213aaaa        <- does this mean it should not compress in this case at all? compressed is longer!
    *
    *  pattern: NON-compressable number + Compressable chars/nums     12333321 -> 124x321 is Wrong!  need: 1x11x24x321
     *
    * */
    public static String compress(String s){
        if (s.length() == 0)
            return s;

//        int c = 1;
//        char p = s.charAt(0);
//        for (int i=1; i)
        return "";
    }

    public static void main(String[] args) {
        System.out.println(compress(""));
        System.out.println(compress(""));
        System.out.println(compress(""));
    }
}