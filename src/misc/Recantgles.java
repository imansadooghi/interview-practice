package misc;

import java.util.ArrayList;

public class Recantgles {

    public static void main(String[] args) {
        ArrayList<String> strings = new ArrayList<String>();
        strings.add("192.168.5.0/24");
        strings.add("192.168.5.0/33");
        strings.add("192.168.5.24");
        strings.add("192.168.5.1/22");
        strings.add("192.168.5.1/2x");



        for (String string : strings) {
            System.out.println(f(string));
        }
    }
    public static boolean f(String cidr){
        int split = cidr.indexOf("/");
        if ( split == -1)
            return false;
        String ip = cidr.substring(0,split);
        String numSt = cidr.substring(split+1,cidr.length());

        int num;
        try {
            num = Integer.parseInt(numSt);
        } catch (Exception e){
            return false;
        }
        if (num < 0 || num >32)
            return false;

        String[] x = ip.split(".");
        if (x.length !=4 )
            return false;

        int[] y = new int[4];

        for (int i=0; i<x.length;i++){
            try {
                y[i] = Integer.parseInt(x[i]);

            } catch (Exception e){
                return false;
            }
        }

        for (int i=0; i<y.length;i++){
            if (y[i]<0 || y[i]>255)
                return false;
        }
        int diff = 32-num;
        for (int i=y.length-1;i>=0;i--){
            if (diff >= 8 && y[i] != 0)
                return false;
            else if (diff >=0 && diff < 8) {
                for (int j = 0; j<diff; j++){
                    if ((y[i] & 1)!= 0 )
                        return false;
                    y[i] = y[i]>>1;
                }
            }
            diff -= 8;
        }

        return true;
    }
}


