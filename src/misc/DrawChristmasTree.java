package misc;

public class DrawChristmasTree {
    public static void main(String[] args) {
        //draw(10);
        drawHollow(10);
    }

    public static void draw(int n) {
        for (int i = 0; i < n; i++) {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < n - i; j++)  // spaces
                sb.append(" ");
            for (int k = 0; k < 2*i + 1; k++)
                sb.append("#");
            System.out.println(sb.toString());
        }
    }

    public static void drawHollow(int n) {
        for (int i = 0; i < n; i++) {
            StringBuilder sb = new StringBuilder();
            for (int j = 0; j < n - i; j++)  // spaces
                sb.append(" ");

            if (i == 0)
                sb.append("*");
             else {
                for (int k = 0; k < 2 * i + 1; k++)
                    sb.append((k == 0 || k == 2 * i) ? "#" : " ");
            }
            System.out.println(sb.toString());
        }
    }
}
