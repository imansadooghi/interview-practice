package misc;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class Game {
    List<Room> rooms;
    int numRooms;
    void initializeGame(int levels,int rpl){
        numRooms = levels*rpl;


    }
    boolean isSolvable(Room st, Room end){
        HashSet<Room> visited = new HashSet();
        return dfs(st,end,visited);
    }

    boolean dfs(Room st, Room end, HashSet<Room> visited){
        visited.add(st);
        for (Room neighbor : st.getOpens()){
            if (neighbor.equals(end))
                return true;
            else if (!visited.contains(neighbor)) {
                if (dfs(neighbor, end, visited))
                    return true;
            }
        }
        return false;
    }
}
