package misc;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertEquals;

// https://www.geeksforgeeks.org/maximum-sum-subarray-sum-less-equal-given-sum/
public class regexTest {
    // shortest Repeating pattern
    public static int srp(String s){
        if (s.length() == 0 )
            return 0;
        int patLen = 1, cur = 0;
        char[] chars = s.toCharArray();
        for (int i = 1; i < chars.length; i++) {
            if (chars[i] != chars[i % patLen])
                patLen = i+1;
        }
        return (chars.length % patLen != 0 ) ? s.length(): patLen ;

    }

    public static void main(String[] args) {
        String s= "";
        Pattern p1 = Pattern.compile("^(.+)\1*$");
        Pattern p2 = Pattern.compile("(.+?)\1*$");
        Matcher m = p2.matcher(s);

        System.out.println(s.length());
        System.out.println(srp(s));

        //assertEquals(maxSumSubarray(new int[]{},), );
    }
}
