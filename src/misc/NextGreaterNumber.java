package misc;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

public class NextGreaterNumber {

    /* https://www.geeksforgeeks.org/find-next-greater-number-set-digits/
    * lc 556
    *  find next greater number using the same digits in the number.
    * */
    public static String nextGreater(String num){
        if (num.length() < 2)
            return num;

        char[] n = num.toCharArray();

        // starting from right. look for first n[iNew] < n[iNew+1]     1232'32  2<3
        int i=n.length-2;
        while (i>=0 && n[i] >= n[i+1])
            i--;

        // if all descending  e.g. 4321
        if (i==-1)
            return num;

        // look for a bigger digit on the right of iNew
        int j = n.length-1;
        while (j>i && n[i] >= n[j])
            j--;

        // swap iNew & jNew
        char temp = n[j];
        n[j] = n[i];
        n[i] = temp;

        // sort the digits on right of iNew. since it is already descending -> reverse to make the ascending
        int l = i+1, r = n.length-1;
        while (l<r){
            temp = n[l];
            n[l++] = n[r];
            n[r--] = temp;
        }

        return new String(n);
    }

    public static void main(String[] args) {
        System.out.println(nextGreater(""));
        System.out.println(nextGreater("1"));
        System.out.println("4321 -> "+nextGreater("4321"));
        System.out.println("1234 -> "+nextGreater("1234"));
        System.out.println("2321 -> "+nextGreater("2321"));
        System.out.println("436543 -> "+ nextGreater("436543"));
        System.out.println("432176542 -> "+nextGreater("432176542"));

    }
}
