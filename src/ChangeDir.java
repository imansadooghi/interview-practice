import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class ChangeDir {

    /*
    *
    *
https://leetcode.com/discuss/interview-question/553454/Facebook-or-Phone-or-Change-Working-Directory

Given current directory and change directory path, return final path.

For Example:

Current                 Change            Output

/                    /facebook           /facebook
/facebook/anin       ../abc/def          /facebook/abc/def
/facebook/instagram   ../../../../.      /

Generally, Facebook want us to solve 2 question in 45 minutes.
Got Rejected after 2 days.

Related problems:

https://leetcode.com/problems/simplify-path/
----------------------------------------------

Solution:

First: add currentDir to Stack. skip "" and .
Then: add changeDir to stack:
    when .. and stack not Empty  => pop
    when . or "" or (.. and stack empty) => ignore
    else push to stack
finally, add stack to arraylist. and print

    *
    * */
    public static String changeDir(String cwd, String cd){
        String[] cur = cwd.split("/");
        Stack<String> stk = new Stack();

        for (String s : cur)
            if (!s.equals(".") && !s.equals(""))
                stk.push(s);

       String[] change = cd.split("/");

        for (String s : change){
            if (s.equals("..") && (!stk.isEmpty()))
                stk.pop();
            // . and "" can be ignored, also .. in case stack in Empty
            else if (s.equals(".") || s.equals("") || s.equals(".."))
                continue;
            else
                stk.push(s);
        }

        List<String> lst = new ArrayList<>(stk);
        return "/" + String.join("/",lst);
    }

    public static void main(String[] args) {
        System.out.println(changeDir("/f/b","../abc/def"));
        System.out.println(changeDir("/f/b/","../abc/def"));
    }
}
