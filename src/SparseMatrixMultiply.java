import java.util.*;

public class SparseMatrixMultiply {

    /*
 * o18
 *  dot product of sparse vectors
https://leetcode.com/discuss/interview-question/124823/

 Solution:
 walk through both vectors -> save non-zero points in a list, either use Point Object or simply a {ind,val} int array

 then this is a two-pointer solution starting from begin of each list -> in case they match -> multiply and add to ans.
 * */
    public static int dotProduct(int[] a, int[] b){
        // build dense lists first
        List<Point> pa = new ArrayList<>();
        List<Point> pb = new ArrayList<>();
        SparseMatrixMultiply sp = new SparseMatrixMultiply();
        for (int i = 0; i <a.length ; i++) {
            if(a[i] != 0)
                pa.add(sp.new Point(i,a[i]));
            if(b[i] != 0)
                pb.add(sp.new Point(i,b[i]));
        }

        int ans = 0;
        int ipa = 0,ipb = 0;
        while(ipa<pa.size() && ipb<pb.size()){
            if (pa.get(ipa).i < pb.get(ipb).i)
                ipa++;
            else if (pb.get(ipb).i < pa.get(ipa).i)
                ipb++;
            else
                ans += pa.get(ipa++).v * pb.get(ipb++).v;
        }

        return ans;
    }

    public static int dotProductHM(int[] a, int[] b){
        // build dense lists first
        Map<Integer,Integer> ma = new HashMap<>();
        Map<Integer,Integer> mb = new HashMap<>();

        for (int i = 0; i <a.length ; i++) {
            if(a[i] != 0)
                ma.put(i,a[i]);
            if(b[i] != 0)
                mb.put(i,b[i]);
        }

        int ans = 0;
        for (int i : ma.keySet())
            ans += mb.getOrDefault(i,0) * ma.get(i);

        return ans;
    }

    /* 2 related questions: lc 311  and o18
Given two sparse matrices A and B, return the result of AB.
You may assume that A's column number is equal to B's row number.

Example:

A = [
  [ 1, 0, 0],
  [-1, 0, 3]
]

B = [
  [ 7, 0, 0 ],
  [ 0, 0, 0 ],
  [ 0, 0, 1 ]
]


     |  1 0 0 |   | 7 0 0 |   |  7 0 0 |
AB = | -1 0 3 | x | 0 0 0 | = | -7 0 3 |
                  | 0 0 1 |

related:
https://leetcode.com/discuss/interview-question/system-design/125306/how-to-store-extremely-large-sparse-matrices

    * */

    // naive way
    public static int[][] multiplyRef(int[][] a, int[][] b) {
        int[][] c = new int[a.length][b[0].length];
        for (int i = 0; i < c.length; i++) {
            for (int j = 0; j < b[0].length; j++) {
                for (int k=0; k<a[0].length;k++)
                    c[i][j] += a[i][k]*b[k][j];
            }
        }
        return c;
    }

    public static int[][] multiply(int[][] a, int[][] b) {
        int[][] c = new int[a.length][b[0].length];
        for (int i = 0; i < c.length; i++) {
            for (int k=0; k<a[0].length;k++)
                if (a[i][k] != 0)
                    for (int j = 0; j < b[0].length; j++) {
                        c[i][j] += a[i][k]*b[k][j];
            }
        }
        return c;
    }

    public static int[][] multiplyDCol(int[][] A, int[][] B) {
        int m = A.length, n = A[0].length, nB = B[0].length;
        int[][] result = new int[m][nB];

        List[] indexA = new List[m];
        for(int i = 0; i < m; i++) {
            List<Integer> numsA = new ArrayList<>();
            for(int j = 0; j < n; j++) {
                if(A[i][j] != 0){
                    numsA.add(j);
                    numsA.add(A[i][j]);
                }
            }
            indexA[i] = numsA;
        }

        for(int i = 0; i < m; i++) {
            List<Integer> numsA = indexA[i];
            for(int p = 0; p < numsA.size() - 1; p += 2) {
                int colA = numsA.get(p);
                int valA = numsA.get(p + 1);
                for(int j = 0; j < nB; j ++) {
                    int valB = B[colA][j];
                    result[i][j] += valA * valB;
                }
            }
        }

        return result;
    }

    //
    public static int[][] multiplyDense(int[][] a, int[][] b) {
        SparseMatrixMultiply sp = new SparseMatrixMultiply();

        List<List<Point>> denseA = new ArrayList<>();
        // only keep non-zero rows. rows with all zero will be skipped!
        List<Integer> rowInds = new ArrayList<>();
        for (int i = 0; i < a.length; i++) {
            List<Point> row = new ArrayList<>();
            for (int j = 0; j < a[0].length; j++) {
                if (a[i][j] != 0)
                    row.add(sp.new Point(j,a[i][j]));
            }
            if (row.size() != 0){
                denseA.add(row);
                rowInds.add(i);
            }
        }

        int[][] c = new int[a.length][b[0].length];

        for (int i = 0; i <denseA.size() ; i++) {
            List<Point> row = denseA.get(i);
            int rowInd = rowInds.get(i); // actual rowIndex.  i here is the non zero row. its actual index is in rowInds
            for (int j = 0; j < row.size(); j++) {
                for (int k = 0; k < b[0].length; k++) {
                    int valB =  b[row.get(j).i][k];    //---> for a[i][x] we need b[x][j]
                    c[rowInd][k] += row.get(j).v * valB;
                }
            }
        }

        return c;
    }

    public  static int[][] multiplyHM(int[][] A, int[][] B) {
        if (A == null || A.length == 0 ||
                B == null || B.length == 0) {
            return new int[0][0];
        }

        int m = A.length;
        int n = A[0].length;
        int l = B[0].length;

        int[][] C = new int[m][l];

        // Step 1: convert the sparse A to dense format
        Map<Integer, Map<Integer, Integer>> denseA = new HashMap<>();
        for (int i = 0; i < m; i++) {
            for (int j = 0; j < n; j++) {
                if (A[i][j] != 0) {
                    if (!denseA.containsKey(i)) {
                        denseA.put(i, new HashMap<>());
                    }
                    denseA.get(i).put(j, A[i][j]);
                }
            }
        }

        // Step 2: convert the sparse B to dense format
        Map<Integer, Map<Integer, Integer>> denseB = new HashMap<>();
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < l; j++) {
                if (B[i][j] != 0) {
                    if (!denseB.containsKey(i)) {
                        denseB.put(i, new HashMap<>());
                    }
                    denseB.get(i).put(j, B[i][j]);
                }
            }
        }

        // Step3: calculate the denseA * denseB
        for (int i : denseA.keySet()) {
            for (int j : denseA.get(i).keySet()) {
                if (!denseB.containsKey(j)) {
                    continue;
                }

                for (int k : denseB.get(j).keySet()) {
                    C[i][k] += denseA.get(i).get(j) * denseB.get(j).get(k);
                }
            }
        }

        return C;
    }

    public static void main(String[] args) {
        int[][] a = {{ 1, 0, 0,5},{-1, 0, 3,3}};
        int[][] b = { { 7, 0, 0 }, { 2, 0, 0 }, { 0, 0, 1 },{ 0, 0, 1 }};
        int[][] c1 = multiplyRef(a,b);
        int[][] c2 = multiply(a,b);
        int[][] c3 = multiplyDense(a,b);
        for (int i = 0; i < c1.length ; i++) {
            System.out.println("ref: "+Arrays.toString(c1[i]));
            System.out.println("act: "+Arrays.toString(c2[i]));
            System.out.println(" hm: "+Arrays.toString(c3[i]));
        }

//        int[] v1 = {1,0,0,0,0,0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,20,0,0,0,0,100};
//        int[] v2 = {1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,30,0,0,0,100,0};
//        System.out.println(dotProduct(v1,v2));
//        System.out.println(dotProductHM(v1,v2));
    }

    public class Point{
        int i, v;
        public Point(int i, int v){
            this.v =v;
            this.i = i;
        }
    }
}
