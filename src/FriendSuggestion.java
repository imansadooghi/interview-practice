import java.util.*;

/*
*
*
*

in Social Network Graph,
                        given Person -> return the Person that has MOST FRIENDS IN COMMON with P

-> so look for someone two nodes away (friend of friend) that has most common with input.

-------------
Solution:
-> go through Input's friends -> add their friends to a map along with count of common with you ->   NOT if they are your direct friend or yourself
-> return one with MAX common



Suppose you have a small, in-memory, social network and each person registered in it is modelled as follow:

Person: {int id, Set<Integer> friends}

method that  will return the person that has most in friends in common with the input 'p'.
In case multiple people have the same number of friends in common, return any one of them.

Suppose the following scenario:

Person A: [B, C]
Person B: [A, D, E]
Person C: [A, D, F]
Person D: [B, C, E]
Person E: [B, D, F]
Person F: [C, E]

When we call friendSuggestion(A) it will return Person D.


*
*
*
*
https://leetcode.com/discuss/interview-question/486188/Facebook-or-Phone-or-Friend-Suggestion
*
* */
public class FriendSuggestion {

    public Person suggestFriend(Person p) {
        int max = -1;
        Person output = null;

        Map<Person, Integer> map = new HashMap<>();

        for (Person friend : p.friends) {
            for (Person suggested : friend.friends) {
                // exlude P himself &   exclude if Suggested is Directly your friend Already
                if (suggested.id != p.id && !p.friends.contains(suggested)) {
                    map.put(suggested, map.getOrDefault(suggested, 0) + 1);
                }
            }
        }

        for (Map.Entry<Person, Integer> mutual : map.entrySet()) {
            if (mutual.getValue() > max) {
                max = mutual.getValue();
                output = mutual.getKey();
            }
        }

        return output;
    }

    private static class Person {

        private int id;
        private Set<Person> friends = new HashSet<>();

        public Person(int id) {
            this.id = id;
        }

        @Override
        public String toString() {
            return "Person{" +
                    "id=" + id +
                    '}';
        }
    }

    public static void main(String[] args) {
        FriendSuggestion problem = new FriendSuggestion();
        problem.test1();
        problem.test2();
    }

    private void test1() {
        Person a = new Person(1);
        Person b = new Person(2);
        Person c = new Person(3);
        Person d = new Person(4);
        Person e = new Person(5);
        Person f = new Person(6);

        a.friends.addAll(Arrays.asList(b, c));
        b.friends.addAll(Arrays.asList(a, d, e));
        c.friends.addAll(Arrays.asList(a, d, f));
        d.friends.addAll(Arrays.asList(b, c, e));
        e.friends.addAll(Arrays.asList(b, d, f));
        f.friends.addAll(Arrays.asList(c, e));

        System.out.println(suggestFriend(a));
    }

    private void test2() {
        Person a = new Person(1);
        Person b = new Person(2);
        Person c = new Person(3);

        a.friends.addAll(Arrays.asList(b, c));
        b.friends.addAll(Arrays.asList(a, c));
        c.friends.addAll(Arrays.asList(a, b));

        System.out.println(suggestFriend(a));
    }
}
