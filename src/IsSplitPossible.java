public class IsSplitPossible {

    /*

is Array splitable in two -> without moving numbers CONTIGUOUS;
first get sum -> assign it to RightSide
then keep adding nums to left and subtract from right
 each time check if left and right are equal


dont confuse with this:
https://leetcode.com/problems/partition-equal-subset-sum/
this one asks for subarrays in any order not-contiguous ->  1,5,11,5 -> true


    * */
    public static boolean isSplitPossible(int[] nums){
        int right = 0, left = 0;
        for (int i : nums)
            right += i;

        for (int i : nums){
            right -= i;
            left +=i;
            if (left == right)
                return true;
        }
        return false;
    }


    public static void main(String[] args) {
        System.out.println(isSplitPossible(new int[]{7,1,2,3,3}));
        System.out.println(isSplitPossible(new int[]{4,1,2,1,2,3,4,1,2,3,4}));
    }
}
