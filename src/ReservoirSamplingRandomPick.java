import java.util.Arrays;
import java.util.Random;

public class ReservoirSamplingRandomPick {
    int[] nums;
    Random rand;

    public ReservoirSamplingRandomPick(int[] nums){
        this.nums = nums;
        rand = new Random();
    }

    public int[] pick(int k){
        return nums;
    }

    // the first k of the nums are the answer here.
    // this one takes O(n-k) ~= O(n)
    // we do two random calls for each.
    //      First to see if this index wins replacing somethig in first k nums
    //      Second, pick the unlucky one that will be replaced
    public int[] pickAny(int k){
        for (int i=k;i<nums.length;i++){
            // if it wins with chance of  k/(k+j)   here: i = k+j-1   e.g k=3 i=3(=4th index)  -> we need 3/4 -> so i+1 is the divident
            if (rand.nextInt(i+1) < k){ // see if number picked from 0 to i(inclusive) fell within 0 to k-1  ->
                // replace it with the unlucky one already picked from 0 - k-1
                int toBeReplaced = rand.nextInt(k);
                int temp = nums[toBeReplaced];
                nums[toBeReplaced] = nums[i];
                nums[i] = temp;
            }

        }
        return nums;
    }

    // O(k) not sure if probability is actually k/n
    // here. the LAST k are the answer!
    public int[] pickAny2(int k) {
        for (int i = 0; i < k ; i++) {
            int replacer = rand.nextInt(nums.length - i) ;   //  replacer from k to end
            int temp = nums[replacer];
            nums[replacer] = nums[nums.length - i -1]; // nums[i] goes from 0 to k-1;
            nums[nums.length - i -1] = temp;
        }
        return nums;
    }

    // Randomly place K mine in MxN Grid
    // https://leetcode.com/discuss/interview-question/124759/

    public char[][] landMines(int m, int n, int k) {
        /*
        idea1: not gonna be k/m*n -> just a way to do this:
        keep picking random number -> if already picked, do again
        while (i < k)
            random pick x between (1-n)    n is for rows
            random pick y between (1-m)    n is for cols
            if (b[x,y] != 'X')
                b[x,y] = 'X'
                i++


        *  idea2: for the first k rows,
        *           for each row, pick 1 col -> 1/n chance for elems in that row
        *
        *        then starting from k+1 row, for each row index i= k+j
        *         with random chance of  k/k+j ->  if win the replacement (// if(rand.nextInt(i+1) < k) )
        *           replace one row from 1 to k.
        *
        * - problem with this approach is:  there will be no rows with more than 1 bomb
        *
        *
        *
idea3:
Use reservoir sampling: k out of mn.
To make it easier to understand, assign an element ID to each grid square 1..mn.
To begin the algo, first k IDs are added to the reservoir of k elements.
Subsequently, go through the rest of the elements (k+1 .. m*n) and generate a random number (r) between 1 and the element ID in the iteration.
If the random number (r) is between 1 and k, replace the rth value in the reservoir with the new element ID.
At the end, the reservoir list will have the element IDs of the grid which need to be mined. So convert these back to the grid indices.

 beginning set of bombs [ 0:0, 0:1, ... , 0:k]
 now walk over rest of matrix:
     0:k+1  -> if wins possibility of  k/k+1  -> then random pick 0 to k-1 and replace (assume index:1 was picked) ->
                                                     [ 0:0, 0:k+1, ... , 0:k]
     ...
        * */

        char[][] grid = new char[m][n];

        return grid;
    }
    public static void main(String[] args) {
        ReservoirSamplingRandomPick rpi = new ReservoirSamplingRandomPick(new int[]{1,2,3,1,2,3,4,5,3,2});
        System.out.println(Arrays.toString(rpi.pickAny(4)));
        System.out.println(Arrays.toString(rpi.pickAny2(4)));
        System.out.println(Arrays.toString(rpi.pickAny(4)));
        System.out.println(Arrays.toString(rpi.pickAny2(4)));


    }
}
