import com.sun.corba.se.impl.orbutil.closure.Future;

import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/*
*  https://leetcode.com/submissions/detail/95331163/
*  https://leetcode.com/problems/design-hit-counter/discuss/83483/Super-easy-design-O(1)-hit()-O(s)-getHits()-no-fancy-data-structure-is-needed!
*  https://leetcode.com/problems/design-hit-counter/discuss/83505/simple-java-solution-with-explanation/445083
*

* */
// TODO: the code is thread safe. make it multi-threaded.
public class HitCounter {
    private final ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
    private final ReentrantReadWriteLock.ReadLock rLock = readWriteLock.readLock();
    private final ReentrantReadWriteLock.WriteLock wLock = readWriteLock.writeLock();

    private int windowSize;
    private int solution;  // 0:array 1:queue 2:TreeMap

    //array solution
    private int[] hits;
    private int[] times;

    // queue solution
    private Queue<Integer> hitsQueue;

    // TreeMap solution   key:timestamp value: total hits so far (from start time)
    private TreeMap<Integer,Integer> windowMap;
    private int total;

    public HitCounter(int windowSize, int solution) {
        switch (solution){
            case 0:
                hits = new int[windowSize];
                times = new int[windowSize];
                break;

            case 1:
                hitsQueue = new LinkedList<>();
                break;

            case 2:
                windowMap = new TreeMap<>();
                total = 0;
                windowMap.put(-300,0);
                break;

            default:
                System.out.println("not a valid solution. pick between:(0,1,2)");
                System.exit(1);
        }

        this.windowSize = windowSize;
        this.solution = solution;
    }

    private HitCounter(){

    }

    public void hit(int timestamp){
        wLock.lock();
        try {
            if (solution == 0) {
                int index = timestamp % windowSize;
                if (times[index] == timestamp) {
                    hits[index]++;
                } else {
                    times[index] = timestamp;
                    hits[index] = 1;
                }
            } else if (solution == 1) {
                hitsQueue.offer(timestamp);
            } else {
                windowMap.put(timestamp,++total);
            }
        } finally {
            wLock.unlock();
        }
    }

    public int getCount(int timestamp){
        rLock.lock();
        int count = 0;
        try {

            if (solution == 0) {
                for (int i = 0; i < hits.length; i++)
                    if (timestamp - times[i] < windowSize)
                        count += hits[i];

            } else if (solution == 1) {
                while (!hitsQueue.isEmpty() && timestamp - hitsQueue.peek() >= 300 )
                    hitsQueue.poll();
                count = hitsQueue.size();
            } else {
                int end = windowMap.get(windowMap.floorKey(timestamp));
                int begin = windowMap.get(windowMap.floorKey(timestamp-300));
                count = end - begin;
            }
        } finally {
            rLock.unlock();
        }

        return count;
    }

    public static void main(String[] args){
        HitCounter hc0 = new HitCounter(300,0);
        HitCounter hc1 = new HitCounter(300,1);
        HitCounter hc2 = new HitCounter(300,2);

        Random rand = new Random();
        int nThreads = 5;
        ExecutorService executorService = Executors.newFixedThreadPool(nThreads);
        java.util.concurrent.Future<?> f = executorService.submit( () -> {
                    int id = Thread.currentThread().getPriority();
                    int total = 200;

                    for (int i = total/nThreads * (id-1); i < total/nThreads * id; i++) {
                        if (rand.nextBoolean())
                            for (int c = 0; c < rand.nextInt(10); c++) {
                                hc0.hit(i);
                                hc1.hit(i);
                                hc2.hit(i);

//                                System.out.println(hc0.getCount(iNew));
//                                System.out.println(hc1.getCount(iNew));
//                                System.out.println(hc2.getCount(iNew));
                            }
                    }
                }
        );

        try {
            f.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        executorService.shutdown();

        System.out.println(hc0.getCount(150));
        System.out.println(hc1.getCount(150));
        System.out.println(hc2.getCount(150));

//        for (int t: hc.times) {
//            System.out.print(t + ", ");
//        }
    }
}
