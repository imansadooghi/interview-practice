import java.util.*;

public class PalindromicSubSequencesAll {

    /*

Given a string s, return all palindromic subsequences of s.

Example:
Input: "abac"
Output: ["a", "b", "c", "aa", "aba"]

    * */


    /*
     *  Idea: build up answers from short ones and keep expanding.
     *
     *  dp[l][r] -> a set that has all answers with from l to r  inclusive
     *
     *  start with Base cases of
     *   l & l -> single char palindromes
     *   l & l-1 -> empty strings ""
     *
     *   the for each case, add potential answers of the following:
     *       answers of l & r-1
     *     answers of l+1 & r
     *    answers of l+1 and r-1
     *    Additionally if s[l] == s[r] -> add answers of l+1 and r-1 again this time with l & r added to both ends
     * */
    private static List<String> palindromicSubseqsRef(String s) {
        if (s == null || s.length() == 0) return Collections.emptyList();
        int len = s.length();
        // dp[i][j] denotes all solutions from left:i to right:j inclusive. s.substring(i,j+1);
        Set<String>[][] dp = new Set[len][len];

        // for every single letter -> add "" and single letter strings
        for (int i=0; i<len; i++) {
            dp[i][i] = new HashSet<>();
            dp[i][i].add(String.valueOf(s.charAt(i)));
            dp[i][i].add("");
        }

        // for each empty -> add ""
        for (int i=1; i<len; i++) {
            dp[i][i-1] = new HashSet<>();
            dp[i][i-1].add("");
        }

        // for every case: add answers with l & r-1  and  answers of l+1 & r  and answers of l+1 and r-1
        for (int j=1; j<len; j++) {
            for (int i=j-1; i>=0; i--) {
                dp[i][j] = new HashSet<>();
                for (String p:   dp[i][j-1])
                    dp[i][j].add(p);
                for (String p:   dp[i+1][j])
                    dp[i][j].add(p);
                for (String p: dp[i+1][j-1])
                    dp[i][j].add(p);
                // if l and r are same => add one  more series of l+1 & r+1  with l & r added to two ends.
                if (s.charAt(i) == s.charAt(j)) {
                    for (String p: dp[i+1][j-1])
                        dp[i][j].add(s.charAt(i) + p + s.charAt(j));
                }
            }
        }

        dp[0][len-1].remove("");
        return new ArrayList<>(dp[0][len-1]);
    }


    public static List<String> palindromicSubseqs(String ss){
        char[] s = ss.toCharArray();
        Set<String>[][] dp = new Set[s.length][s.length];

        for (int l=s.length-1;l>=0;l--){
            for (int r=l;r<s.length;r++){
                dp[l][r] = new HashSet<>();
                dp[l][r].add("");
                if (l == r) {
                    dp[l][l].add(String.valueOf(s[l]));
                }else if (s[l] == s[r]){
                    if (r-l == 1) {
                        dp[l][r].add(ss.substring(l,r+1));
                        dp[l][r].add(String.valueOf(s[l]));
                    }else {
                        for (String pal : dp[l+1][r-1]) {
                            dp[l][r].add(s[l] + pal + s[r]);
                            dp[l][r].add(pal);
                        }
                    }
                } else {
                    for (String pal : dp[l+1][r])
                        dp[l][r].add(pal);
                    for (String pal : dp[l][r-1])
                        dp[l][r].add(pal);
                }
            }
        }

        dp[0][s.length-1].remove("");
        return new ArrayList<String>(dp[0][s.length-1]);
    }


    public static void main(String[] args) {
        System.out.println(palindromicSubseqs("abcaaccaccac"));
        String[] strs = {"abac","aabac","aabaca","sdfnasesaa","a","abcaac","abcaacaaaaaacccccaccacaaaaaaac"};
        for (String s: strs) {
            List<String> expected = palindromicSubseqsRef(s);
            List<String> actual = palindromicSubseqs(s);
            System.out.println(expected.containsAll(actual) && actual.containsAll(expected));
        }

        //  System.out.println(palindromicSubseqs("aabaca"));

    }

}
